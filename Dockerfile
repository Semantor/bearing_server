FROM openjdk:11.0.11-jdk
ARG JAR_FILE=backend-app/build/libs/bearing-server-*.jar
ARG OPTIONS=/home/fred/Yandex.Disk/ls/options.properties
ARG ADDITIONAL=/home/fred/Yandex.Disk/ls
COPY ${JAR_FILE} /app/app.jar
COPY ${OPTIONS} /app/options.properties
COPY ${ADDITIONAL} /ls
ENTRYPOINT ["java","-jar","/app.jar","--spring.config.location=/app/options.properties"]