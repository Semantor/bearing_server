package org.bitbucket.semantor.config;

import org.bitbucket.semantor.repository.ImageRepository;
import org.bitbucket.semantor.repository.impls.LocalHashedImageRepositoryImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
//@PropertySources({@PropertySource("application-${spring.profiles.active}.properties"),@PropertySource("${path.to.path}")})
@PropertySource("application-${spring.profiles.active}.properties")
class BeanRepositoryConfig {

    @Value("${path.to.images}")
    private String pathToImages;
    @Value("${path.to.price}")
    private String pathToPrice;
    @Value("${path.to.mail}")
    private String pathToMail;
    @Value("${path.to.order}")
    private String pathToOrder;
    @Value("${path.to.user}")
    private String pathToUser;


    @Bean
    ImageRepository imageRepository() {
        return new LocalHashedImageRepositoryImpl(pathToImages);
    }

}
