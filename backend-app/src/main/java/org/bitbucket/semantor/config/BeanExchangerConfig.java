package org.bitbucket.semantor.config;

import org.bitbucket.semantor.exchanger.MailExchanger;
import org.bitbucket.semantor.exchanger.OrderExchanger;
import org.bitbucket.semantor.exchanger.UserExchanger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.bitbucket.semantor.exchanger.BearingExchanger;

@Configuration
public class BeanExchangerConfig {

    @Bean
    BearingExchanger bearingExchanger() {
        return new BearingExchanger();
    }

    @Bean
    MailExchanger mailExchanger(OrderExchanger orderExchanger) {
        return new MailExchanger(orderExchanger);
    }

    @Bean
    OrderExchanger orderExchanger(UserExchanger userExchanger) {
        return new OrderExchanger(userExchanger);
    }

    @Bean
    UserExchanger userExchanger() {
        return new UserExchanger();
    }
}
