package org.bitbucket.semantor.config;


import org.bitbucket.semantor.exchanger.BearingExchanger;
import org.bitbucket.semantor.exchanger.OrderExchanger;
import org.bitbucket.semantor.exchanger.UserExchanger;
import org.bitbucket.semantor.data.object.UserHandler;
import org.bitbucket.semantor.repository.*;
import org.bitbucket.semantor.repository.specs.BearingSpecs;
import org.bitbucket.semantor.repository.specs.UserReviewSpecs;
import org.bitbucket.semantor.repository.specs.UserSearchSpecs;
import org.bitbucket.semantor.services.*;
import org.bitbucket.semantor.services.impls.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class DevBeanServiceConfig {


    @Bean
    @Profile("dev")
    BearingSearchService bearingSearchService(BearingSpecs bearingSpecs,
                                              BearingRepository bearingRepository,
                                              BearingExchanger bearingExchanger,
                                              UserSearchRecorder userSearchRecorder) {
        return new BearingSearchServiceImpl(bearingSpecs, bearingRepository, bearingExchanger, userSearchRecorder);
    }

    @Bean
    @Profile("dev")
    BearingSupplyAndRecordUserActivityService bearingSupplyAndRecordUserActivityService(BearingRepository bearingRepository,
                                                                                        UserReviewRecorder userReviewRecorder,
                                                                                        BearingExchanger bearingExchanger,
                                                                                        UserIdChecker userIdChecker) {
        return new BearingSupplyAndRecordUserActivityServiceImpl(bearingRepository, userReviewRecorder, bearingExchanger,userIdChecker);
    }

    @Bean
    @Profile("dev")
    ImageService imageService(ImageRepository imageRepository) {
        return new ImageServiceImplWithExactMatch(imageRepository);
    }

    @Bean
    @Profile("dev")
    OrderRecorderWithActivityService orderRecorderWithActivityService(OrderRepository orderRepository,
                                                                      OrderExchanger orderExchanger,
                                                                      UserRepository userRepository,
                                                                      UserIdChecker userIdChecker) {
        return new OrderRecorderWithActivityServiceImpl(orderRepository, orderExchanger, userRepository,userIdChecker);
    }

    @Bean
    @Profile("dev")
    OrderSupplyAndRecordActivityService orderSupplyAndRecordActivityService(OrderRepository orderRepository,
                                                                            OrderExchanger orderExchanger,
                                                                            UserRepository userRepository) {
        return new OrderSupplyAndRecordActivityServiceImpl(orderRepository, orderExchanger, userRepository);
    }

    @Bean
    @Profile("dev")
    UserReviewRecorder userReviewRecorder(UserReviewRepository userReviewRepository,
                                          UserRepository userRepository,
                                          BearingRepository bearingRepository) {
        return new UserReviewRecorderImpl(userReviewRepository, userRepository, bearingRepository);
    }

    @Bean
    @Profile("dev")
    UserReviewSupplier userReviewSupplier(UserReviewRepository userReviewRepository,
                                          UserRepository userRepository,
                                          UserIdChecker userIdChecker,
                                          UserReviewSpecs userReviewSpecs) {
        return new UserReviewSupplierImpl(userReviewRepository,userRepository,userIdChecker,userReviewSpecs);
    }

    @Bean
    @Profile("dev")
    UserSearchRecorder userSearchRecorder(UserSearchRepository userSearchRepository,
                                          UserIdChecker userIdChecker,
                                          UserRepository userRepository) {
        return new UserSearchRecorderImpl(userSearchRepository,userIdChecker,userRepository);
    }

    @Bean
    @Profile("dev")
    UserSearchSupplier userSearchSupplier(UserIdChecker userIdChecker,
                                          UserRepository userRepository,
                                          UserSearchRepository userSearchRepository,
                                          UserSearchSpecs userSearchSpecs) {
        return new UserSearchSupplierImpl(userIdChecker,userRepository, userSearchRepository,userSearchSpecs);
    }

    @Bean
    @Profile("dev")
    UserServiceWithRolePermission userServiceWithRolePermission(UserRepository userRepository,
                                                                UserIdChecker userIdChecker,
                                                                UserExchanger userExchanger,
                                                                UserHandler userHandler){
        return new UserServiceWithRolePermissionImpl(userRepository,userIdChecker,userExchanger,userHandler);
    }

}
