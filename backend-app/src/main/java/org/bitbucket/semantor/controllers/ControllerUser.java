package org.bitbucket.semantor.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.semantor.dto.OrderDTO;
import org.bitbucket.semantor.services.OrderSupplyAndRecordActivityService;
import org.bitbucket.semantor.services.UserServiceWithRolePermission;
import org.springframework.web.bind.annotation.*;
import org.bitbucket.semantor.dto.UserDTO;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/users")
@Slf4j
public class ControllerUser {
    private final UserServiceWithRolePermission userService;
    private final OrderSupplyAndRecordActivityService orderService;

    @GetMapping
    public List<UserDTO> getAllUsers(@RequestHeader(name = "user") String userID) {
        log.info(String.format("--User : %s : request to get all users ", userID));
        return userService.getAllUser(userID);
    }

    @GetMapping("/{id}")
    public UserDTO getUserByID(@PathVariable String id,
                               @RequestHeader(name = "user",required = false) String userID) {
        log.info(String.format("--User : %s : request to get user with id = %s ", userID,id));
        return userService.getUserByID(id,userID);
    }

    @PostMapping
    public UserDTO createNewUser(UserDTO userDTO,
                                 @RequestHeader(name = "target_user", required = false) String targetUserID,
                                 @RequestHeader(name = "requesting_user",required = false) String userID) {
        log.info(String.format("--User : %s : request to create user: %s ", userID,userDTO.toString()));
        return userService.saveUser(userDTO,targetUserID,userID);
    }

    @GetMapping("/{id}/orders")
    public List<OrderDTO> getUserOrder(@PathVariable(name = "id") String id,
                                       @RequestHeader(name = "user",required = false) String userID) {
        log.info(String.format("--User : %s : request to get users orders with user id =  %s ", userID,id));
        return orderService.getUserOrder(id, userID);
    }

}
