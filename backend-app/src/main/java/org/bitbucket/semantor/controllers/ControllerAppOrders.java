package org.bitbucket.semantor.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.semantor.dto.OrderDTO;
import org.bitbucket.semantor.services.OrderRecorderService;
import org.springframework.web.bind.annotation.*;
import org.bitbucket.semantor.services.OrderSupplierService;

@RestController
@AllArgsConstructor
@RequestMapping("/app/orders")
@Slf4j
public class ControllerAppOrders {
    private final OrderRecorderService managerOrder;
    private final OrderSupplierService orderSupplierService;


    @GetMapping("/{id}")
    public OrderDTO getDeviceById(@PathVariable("id") String id, @RequestHeader(name = "user") String userID) {
        log.info(String.format("--User : %s : request to get order with id = %s", userID, id));
        return orderSupplierService.getOrderById(Integer.parseInt(id));
    }

    @PostMapping
    public boolean reserveOrder(@RequestBody OrderDTO order,@RequestHeader(name = "user") String userID) {
        log.info(String.format("--User : %s : request to get reserve new order: %s", userID, order.toString()));
        return managerOrder.reserveOrder(order);
    }

}
