package org.bitbucket.semantor.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.semantor.dto.BearingDTO;
import org.bitbucket.semantor.services.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/bearings")
@Slf4j
public class BearingSiteController {
    private final BearingSupplyAndRecordUserActivityService bsauars;
    private final SearchBearingAndRecordActivityService sbaras;

    @GetMapping
    public String bearingsPage(Model model, @RequestHeader(name = "user", required = false) String userID) {
        log.info(String.format("--User : %s : request to get all bearings", userID));
        List<BearingDTO> allBearings = bsauars.getAllBearings(userID);
        model.addAttribute("bearings",
                allBearings);
        return "bearings";
    }

    @GetMapping("/{id}")
    public String bearingPage(Model model, @PathVariable String id,
                              @RequestHeader(name = "user", required = false) String userID) {
        log.info(String.format("--User : %s : request to get bearing with id = %s", userID, id));
        model.addAttribute("el",
                bsauars.getBearingById(id,userID));
        return "bearing";
    }

    @GetMapping("/search")
    public String bearingSearchPage(Model model, @RequestParam(name = "key") String key,
                                    @RequestHeader(name = "user", required = false) String userID) {
        log.info(String.format("--User : %s : request to search bearing with key = %s", userID, key));
        model.addAttribute("results", sbaras.searchByNameAndAlias(key,userID));
        return "search";
    }


    @PostMapping
    public String searchBearing(@RequestParam String search, Model model) {
        log.info("--redirect from /bearings to search");
        return "redirect:/bearings/search?key=" + search;
    }
}
