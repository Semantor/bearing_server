package org.bitbucket.semantor.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.bitbucket.semantor.services.ImageService;

@RestController
@AllArgsConstructor
@Slf4j
public class ControllerImages {
    private final ImageService imageService;

    @GetMapping(value = "/image/{name}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getBearingPhoto(@PathVariable String name,
                                  @RequestHeader(name = "user",required = false) String userID) {
        log.info(String.format("--User : %s : request to get image with name = %s", userID, name));
        return imageService.getImage(name);
    }
}
