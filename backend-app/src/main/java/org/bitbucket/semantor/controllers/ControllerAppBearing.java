package org.bitbucket.semantor.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.semantor.dto.BearingDTO;
import org.bitbucket.semantor.services.BearingSearchService;
import org.bitbucket.semantor.services.BearingSupplyAndRecordUserActivityService;
import org.bitbucket.semantor.services.SearchBearingAndRecordActivityService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/app/bearings")
@Slf4j
public class ControllerAppBearing {

    private final BearingSupplyAndRecordUserActivityService bsauars;
    private final BearingSearchService bearingSearchService;

    @GetMapping
    public List<BearingDTO> getAllBearing(@RequestHeader(name = "user", required = false) String userID) {
        log.info(String.format("--User : %s : request to get all bearings", userID));
        return bsauars.getAllBearings(userID);
    }

    @GetMapping("/{id}")
    public BearingDTO getBearingById(@RequestParam String id,
                                     @RequestHeader(name = "user", required = false) String userID) {
        log.info(String.format("--User : %s : request to get bearing with id = %s", userID, id));
        return bsauars.getBearingById(id, userID);
    }

    @GetMapping("/search")
    public List<BearingDTO> searchBearings(@RequestHeader(name = "user", required = false) String userID,
                                           @RequestParam(name = "key", required = false) String key,
                                           @RequestParam(name = "bearingID", required = false) Integer bearingID,
                                           @RequestParam(name = "name", required = false) String name,
                                           @RequestParam(name = "fullname", required = false) String fullname,

                                           @RequestParam(name = "priceMin", required = false) String priceMin,
                                           @RequestParam(name = "priceMax", required = false) String priceMax,

                                           @RequestParam(name = "din", required = false) String din,
                                           @RequestParam(name = "dinMin", required = false) String dinMin,
                                           @RequestParam(name = "dinMax", required = false) String dinMax,

                                           @RequestParam(name = "dout", required = false) String dout,
                                           @RequestParam(name = "doutMin", required = false) String doutMin,
                                           @RequestParam(name = "doutMax", required = false) String doutMax,

                                           @RequestParam(name = "a", required = false) String a,
                                           @RequestParam(name = "aMin", required = false) String aMin,
                                           @RequestParam(name = "aMax", required = false) String aMax,


                                           @RequestParam(name = "manufacturer", required = false) String manufacturer,
                                           @RequestParam(name = "alias", required = false) String alias,
                                           @RequestParam(name = "description", required = false) String description,
                                           @RequestParam(name = "mark", required = false) String mark
    ) {

        return bearingSearchService.search(userID, key, bearingID, name, fullname,
                priceMin, priceMax, din, dinMin, dinMax, dout, doutMin, doutMax, a, aMin, aMax,
                manufacturer, alias, description, mark);
    }

}
