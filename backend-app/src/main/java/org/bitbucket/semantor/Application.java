package org.bitbucket.semantor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class Application {
    public static void main(String[] args) {
        log.info("Incoming args:");
        for (String arg :
                args) {
            log.info(arg);
        }
        SpringApplication app = new SpringApplication(Application.class);
        app.run(args);
    }

}