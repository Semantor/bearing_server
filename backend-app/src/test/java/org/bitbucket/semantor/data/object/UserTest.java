package org.bitbucket.semantor.data.object;

import org.gradle.internal.impldep.org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.validation.annotation.Validated;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
class UserTest {

    private Validator validator;

    @BeforeEach
    void init() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @Test
    void positiveTest(){
        User name = User.builder().name("name").phone("9876543210").build();
        Set<ConstraintViolation<User>> validate = validator.validate(name);
        validate.forEach(System.out::println);
        assertTrue(validate.isEmpty());
    }

    @Test
    void testPhoneWithWrongFirstNumber() {
        User vasilii = User.builder().phone("789456123").name("Vasilii").build();
        Set<ConstraintViolation<User>> validate = validator.validate(vasilii);
        assertFalse(validate.isEmpty());
    }
    @Test
    void testNameWith3Symbols(){
        User vas = User.builder().phone("9876543210").name("vas").build();
        Set<ConstraintViolation<User>> validate = validator.validate(vas);
        assertFalse(validate.isEmpty());
    }



    @Test
    void testName() {
        User build = User.builder().phone("9876453210").build();
        System.out.println(build);
    }
}