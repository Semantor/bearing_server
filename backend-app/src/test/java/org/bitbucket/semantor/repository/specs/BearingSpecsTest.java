package org.bitbucket.semantor.repository.specs;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BearingSpecsTest {
    private final BearingSpecs bearingSpecs = new BearingSpecs();

    @Test
    void checkForDouble9() {
        assertTrue(bearingSpecs.checkForDouble("9"));
    }

    @Test
    void checkForDouble9point0() {
        assertTrue(bearingSpecs.checkForDouble("9.0"));
    }

    @Test
    void checkForDoubleminus9point0() {
        assertTrue(bearingSpecs.checkForDouble("-9.0"));
    }

    @Test
    void checkForDoubleTrashWithCharacter() {
        assertFalse(bearingSpecs.checkForDouble("-9a.0"));
    }

    @Test
    void checkForDoubleTrashWithOInsteadZero() {
        assertFalse(bearingSpecs.checkForDouble("-9.O"));
    }


}