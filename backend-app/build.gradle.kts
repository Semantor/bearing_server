plugins {
    `kotlin-dsl`
    java
    application
    id("org.springframework.boot") version Version.springBoot
}

apply(plugin = "io.spring.dependency-management")
val appName = "bearing-server"
val appVer = "0.0.1"
group = "org.bitbucket.semantor"
version = appVer
application {
    mainClass.set("org.bitbucket.semantor.Application")
}

tasks.getByName<org.springframework.boot.gradle.tasks.bundling.BootJar>("bootJar") {
    this.archiveFileName.set("${appName}-${archiveVersion.get()}.${archiveExtension.get()}")
}

repositories {
    mavenCentral()
}

configurations.forEach {
    it.exclude("org.springframework.boot", "spring-boot-starter-logging")
    it.exclude("ch.qos.logback", "logback-classic")
}
dependencies {
    implementation(Libs.jetBrainsAnnotation)
    implementation(project(":model"))
    implementation(project(":backend"))
    implementation(Libs.commonIO)
    implementation(Libs.springBootStarterTest)
    implementation(Libs.springBootStarterWeb)
    implementation(Libs.springBootActuator)
    implementation(Libs.springBootStarterValidation)
    implementation(Libs.springBootHateoas)
    implementation(Libs.thymeleaf)
    testImplementation(Libs.h2)
    implementation(Libs.postgresJdbcDriver)
    implementation(Libs.springJpa)
    implementation(Libs.jpaModelGen)
    implementation("commons-io:commons-io:2.8.0")
    compileOnly(Libs.lombok)
    testCompileOnly(Libs.lombok)
    annotationProcessor(Libs.lombok)
    testAnnotationProcessor(Libs.lombok)

    implementation("org.springframework.boot:spring-boot-starter-log4j2:${Version.springBoot}")
}

tasks.test {
    useJUnitPlatform()
}


