# Requirements to server
1. HighLoad
1. Low Latency
1. Availability
1. Simple



# Cacheable:
1. Bearing

# TODO 
1. Resource with HAL instead DTO
2. ResourceAssemblerSupport instead exchanger
4. Docker Autobuilds.
5. Page/sort
6. Docker Secrets or something another.
7. Export model in external repo.
8. DELETE/PUT user/order
9. Actuator
10. Single request ID
11. Switch properties to multi-document YAML
12. Logging.
13. PropertySource to ConfigurationProperties with prefix
14. User page.
15. User comment in [User](model/src/main/java/org/bitbucket/semantor/data/object/User.java)
16. Set lists to sets in [Order](model/src/main/java/org/bitbucket/semantor/data/object/Order.java)
17. Daemon to cache and refresh.
18. Mail System
19. Zero Image for bearing
20. Repository throws Exceptions due to connection problem
21. Security
22. Profiler and metrics
23. BearingDTOdForList
24. Metadata for spring properties
25. Returning 404 when id doesn't exist
26. @ResponceStatus(HttpStatus.Created) on Post method
27. Split post/put/patch

#Feature
1. Spring HATEOAS(@ResourceSupport,@ResourceAssemblerSupport,@Relation)
2. Spring Data REST
3. Loyalty program
4. Reactive (WebFlux)

#Done
1. Search on web
1. Modularity
1. BearingPictureRepository.
1. Create dat files on app start.
1. External property files.
1. Profiles.
1. Profiles group.

#Controller structure:
##site
```
/bearings CONTROLLER
         GET 
         *header* UUID user not required
         bearings template
         
         POST
         *request* STRING KEY
         redirect search template with request param key
         
         /{id} 
              GET 
              *path* id bearingID
              *header* UUID user not required
              bearing template and create UserReview
         
         /search 
                GET
                *request* STRING key
                *header* UUID user not required
                search template and create UserSearch
                
/images RESTCONTROLLER
       /{name}
              GET
              *path* STRING name
              *header* UUID user
              return byte[] of images with target name                
 ```
##android
```

/app
    /bearings
            GET 
            *header* UUID user
            all bearings
            
            
            /search
            GET
            *requestheader* user String userID
            *requestpatam* STRING key for name, fullname, din, dout, a, alias, mark, description, manufacturer
            *requestpatam* STRING din 
            *requestpatam* STRING dinMin 
            *requestpatam* STRING dinMax 
            *requestpatam* STRING dout 
            *requestpatam* STRING doutMin 
            *requestpatam* STRING doutMax 
            *requestpatam* STRING a
            *requestpatam* STRING aMin
            *requestpatam* STRING aMax
            *requestpatam* String manufacturer
            *requestpatam* STRING priceMin
            *requestpatam* STRING priceMax
            *requestpatam* bearindID Integer bearingID
            requestheader BearingDTO and create UserSearch
            
            /{id} 
            GET 
            *path* STRING bearingID
            *header* UUID user 
            return BearingDTO and create UserReview
            
    /orders RESTCONTROLLER
            POST 
            *body* ORDER
            *header* UUID user who create odrder
            create new order
       
            /{id} 
                GET 
                *path* UUID order
                *header* UUID user
                return order by id
                
            /{id}
                DELETE
                *path* UUID order
                *header* UUID user
                
            /{id}
                PUT
                *path* UUID order
                *header* UUID user
                *body* order
                
    /users RESTCONTROLLER
          GET 
          *header* UUID user
          get all Users
       
          POST 
          *body* USER 
          create new User
       
          /{id} 
          GET 
          *path* UUID user
          return user by uuid
       
          /{id}/orders
          GET
          *path* UUID user
          return user's orders
```
 
