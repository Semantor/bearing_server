package org.bitbucket.semantor.repository;

import org.bitbucket.semantor.data.object.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;
import org.springframework.lang.NonNull;
import org.springframework.lang.NonNullApi;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID>, JpaSpecificationExecutor<User> {
    @Override
    @NotNull
    List<User> findAllById(@NotNull Iterable<UUID> uuids);

    @Override
    @NotNull
    Page<User> findAll(@NotNull Pageable pageable);

    @Override
    @NotNull
    List<User> findAll();

    @Override
    <S extends User> @NotNull S save(@NotNull S entity);

    @Override
    @NotNull
    Optional<User> findById(@NotNull UUID uuid);

    @Override
    @NotNull
    Optional<User> findOne(Specification<User> spec);

    @Override
    @NotNull
    List<User> findAll(Specification<User> spec);

    @Override
    @NotNull
    Page<User> findAll(Specification<User> spec, @NotNull Pageable pageable);

    @Override
    @NotNull
    List<User> findAll(Specification<User> spec, @NotNull Sort sort);

    @Override
    long count(Specification<User> spec);
}
