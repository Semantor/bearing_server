package org.bitbucket.semantor.repository;

import org.bitbucket.semantor.data.object.User;
import org.bitbucket.semantor.data.object.UserReview;
import org.bitbucket.semantor.data.object.catalog.Item;

import java.util.Date;
import java.util.List;

public interface UserActionRepository {
    /**
     * returns all user reviews connected with given item
     * return empty list if there is no reviews or problem occurred
     *
     * @param item for search
     * @return list of users
     */
    List<UserReview> getUserReviewByItem(Item item);

    /**
     * returns user reviews that a given user has made
     * return empty list if there is no reviews or problem occurred
     *
     * @param user
     * @return list of item
     */
    List<UserReview> getUserReviewByUser(User user);

    /**
     * return all users reviews for target time interval
     * return empty list if there is no action or problem occurred
     *
     * @param from date
     * @param to   date
     * @return {@link UserReview}
     */
    List<UserReview> getUserReviewByDate(Date from, Date to);


    /**
     * returns reviews that a given user has made in target time interval
     * return empty list if there is no items or problem occurred
     *
     * @param user
     * @param from date
     * @param to   date
     * @return list of items
     */
    List<UserReview> getUserReviewByDateAndUser(User user, Date from, Date to);

    /**
     * returns reviews connected with this item in the target time interval
     * return empty list if there is no users or problem occurred
     *
     * @param item
     * @param from date
     * @param to   date
     * @return list of users
     */
    List<UserReview> getUserReviewByDateAndItem(Item item, Date from, Date to);

    /**
     * return all reviews connected with given item, which target user has made
     * return empty list if there is no users or problem occurred
     *
     * @param user
     * @param item
     * @return
     */
    List<UserReview> getUserReviewByUserAndItem(User user, Item item);

}
