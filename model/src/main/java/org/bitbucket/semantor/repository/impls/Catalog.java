package org.bitbucket.semantor.repository.impls;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public abstract class Catalog<T> {
    private String destination;
    private List<T> data;


    private Catalog() {
        data=new ArrayList<>();
        loadInformation();
    }

    private void loadInformation() {
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(destination)))) {
            this.data = (List<T>) ois.readObject();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void saveInformation() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(destination)))) {
            oos.writeObject(data);
            System.out.println("Writing is complete");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<T> getData() {
        return data;
    }

    public void addInventoryItem (T object){
        data.add(object);
    }


}
