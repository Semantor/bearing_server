package org.bitbucket.semantor.repository;

import org.bitbucket.semantor.data.object.UserReview;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface UserReviewRepository extends JpaRepository<UserReview, Long>, JpaSpecificationExecutor<UserReview> {

    @Override
    @NotNull
    List<UserReview> findAll();

    @Override
    @NotNull
    List<UserReview> findAll(@NotNull Sort sort);

    @Override
    <S extends UserReview> @NotNull List<S> saveAll(@NotNull Iterable<S> entities);

    @Override
    <S extends UserReview> @NotNull S save(@NotNull S entity);

    @Override
    @NotNull
    Optional<UserReview> findById(@NotNull Long aLong);

    @Override
    boolean existsById(@NotNull Long aLong);

    @Override
    <S extends UserReview> @NotNull Page<S> findAll(@NotNull Example<S> example, @NotNull Pageable pageable);

    @Override
    @NotNull
    Optional<UserReview> findOne(Specification<UserReview> spec);

    @Override
    @NotNull
    List<UserReview> findAll(Specification<UserReview> spec);

    @Override
    @NotNull
    Page<UserReview> findAll(Specification<UserReview> spec, @NotNull Pageable pageable);

    @Override
    @NotNull
    List<UserReview> findAll(Specification<UserReview> spec, @NotNull Sort sort);

    @Override
    long count(Specification<UserReview> spec);
}
