package org.bitbucket.semantor.repository.impls;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.semantor.repository.UserRepository;
import org.bitbucket.semantor.data.object.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
public class UserDB implements UserRepository {
    private final String FILE_NAME;
    private List<User> data;

    public UserDB(String pathToFile) {
        FILE_NAME=pathToFile;
        data = new ArrayList<>();
        loadInformation();
    }

    private void loadInformation() {
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(FILE_NAME)))) {
            this.data = (List<User>) ois.readObject();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void saveInformation() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(FILE_NAME)))) {

            oos.writeObject(data);
            System.out.println("Writing is complete");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<User> findAll() {
        return data;
    }

    @Override
    public User save(User user) {
        data.add(user);
        return user;
    }


    @Override
    public Optional<User> findByPhone(String phone) {
        log.error("Not implemented yet");
        throw new IllegalStateException("Not implemented yet");
    }

    @Override
    public List<User> findByName(String name) {
        log.error("Not implemented yet");
        throw new IllegalStateException("Not implemented yet");
    }
}
