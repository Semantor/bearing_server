package org.bitbucket.semantor.repository.specs;

import org.bitbucket.semantor.data.object.City;
import org.bitbucket.semantor.data.object.City_;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public class CitySpecs {

    public Specification<City> search(String nameLike,
                                      List<Long> ids) {
        Specification<City> specification = Specification.where(null);
        if (nameLike != null)
            specification.and(getCityByName(nameLike));
        if (ids != null)
            specification.and(getCityByIds(ids));
        return specification;
    }

    public Specification<City> getCityByName(String nameLike) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(City_.name), nameLike);
    }

    public Specification<City> getCityByIds(List<Long> ids) {
        return (root, query, criteriaBuilder) -> root.get(City_.id).in(ids);
    }

}
