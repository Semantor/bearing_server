package org.bitbucket.semantor.repository.specs;

import org.bitbucket.semantor.data.object.catalog.Bearing;
import org.bitbucket.semantor.data.object.catalog.Bearing_;
import org.springframework.data.jpa.domain.Specification;

import java.util.regex.Pattern;

public class BearingSpecs {

    /**
     * The border inclusive
     * keyword searching in name, fullname, din, dout, a, manufacturer, alias, mark, description.
     * wrap string value with {@link BearingSpecs#wrap(String)}
     *
     */
    public Specification<Bearing> search(String keyword,
                                         Integer id, String name, String fullname,
                                         Double priceMin, Double priceMax,
                                         Double din, Double dinMin, Double dinMax,
                                         Double dout, Double doutMin, Double doutMax,
                                         Double a, Double aMin, Double aMax,
                                         String manufacturer,
                                         String description,
                                         String alias,
                                         String mark) {
        Specification<Bearing> result = Specification.where(null);
        if (id != null) result.and(getBearingById(id));
        if (name != null) result.and(getBearingByName(wrap(name)));
        if (fullname != null) result.and(getBearingByFullName(wrap(fullname)));
        if (priceMin != null) result.and(getBearingByPriceMin(priceMin));
        if (priceMax != null) result.and(getBearingByPriceMax(priceMax));
        if (din != null) result.and(getBearingByDin(din));
        if (dinMin != null) result.and(getBearingByDinMin(dinMin));
        if (dinMax != null) result.and(getBearingByDinMax(dinMax));

        if (dout != null) result.and(getBearingByDout(dout));
        if (doutMin != null) result.and(getBearingByDoutMin(doutMin));
        if (doutMax != null) result.and(getBearingByDoutMax(doutMax));


        if (a != null) result.and(getBearingByA(a));
        if (aMin != null) result.and(getBearingByAMin(aMin));
        if (aMax != null) result.and(getBearingByAMax(aMax));

        if (manufacturer != null) result.and(getBearingByManufacturer(wrap(manufacturer)));
        if (description != null) result.and(getBearingByDescription(wrap(description)));
        if (alias != null) result.and(getBearingByAlias(wrap(alias)));
        if (mark != null) result.and(getBearingByMark(wrap(mark)));

        Specification<Bearing> additional = Specification.where(null);
        if (keyword!=null) {
            additional.or(getBearingByName(keyword));
            additional.or(getBearingByFullName(keyword));
            additional.or(getBearingByManufacturer(keyword));
            additional.or(getBearingByDescription(keyword));
            additional.or(getBearingByAlias(keyword));
            additional.or(getBearingByMark(keyword));

            if (checkForDouble(keyword)){
                double v = Double.parseDouble(keyword);
                additional.or(getBearingByDin(v));
                additional.or(getBearingByDout(v));
                additional.or(getBearingByA(v));
            }
        }

        return result.and(additional);
    }

    private String wrap(String source){
        return String.format("%%%s%%",source);
    }

    private static final Pattern isDouble = Pattern.compile("-?\\d+(\\.\\d+)?");
    boolean checkForDouble(String s){
        return isDouble.matcher(s).matches();
    }

    private Specification<Bearing> getBearingByDescription(String description) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(Bearing_.description), description);
    }


    public Specification<Bearing> getBearingById(Integer id) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(Bearing_.id), id);
    }

    public Specification<Bearing> getBearingByName(String name) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(Bearing_.name), name);
    }

    public Specification<Bearing> getBearingByFullName(String fullname) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(Bearing_.fullname), fullname);
    }

    public Specification<Bearing> getBearingByPriceMin(Double priceMin) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get(Bearing_.price), priceMin);
    }

    public Specification<Bearing> getBearingByPriceMax(Double priceMax) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(Bearing_.price), priceMax);
    }

    public Specification<Bearing> getBearingByDin(Double din) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(Bearing_.din), din);
    }

    public Specification<Bearing> getBearingByDinMin(Double din) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get(Bearing_.din), din);
    }

    public Specification<Bearing> getBearingByDinMax(Double din) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(Bearing_.din), din);
    }

    public Specification<Bearing> getBearingByDout(Double dout) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(Bearing_.dout), dout);
    }

    public Specification<Bearing> getBearingByDoutMin(Double dout) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get(Bearing_.dout), dout);
    }

    public Specification<Bearing> getBearingByDoutMax(Double dout) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(Bearing_.dout), dout);
    }

    public Specification<Bearing> getBearingByA(Double a) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(Bearing_.a), a);
    }

    public Specification<Bearing> getBearingByAMin(Double a) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get(Bearing_.a), a);
    }

    public Specification<Bearing> getBearingByAMax(Double a) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(Bearing_.a), a);
    }

    public Specification<Bearing> getBearingByManufacturer(String manufacturer) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(Bearing_.manufacturer), manufacturer);
    }

    public Specification<Bearing> getBearingByAlias(String alias) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(Bearing_.alias), alias);
    }

    public Specification<Bearing> getBearingByMark(String mark) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(Bearing_.mark), mark);
    }
}
