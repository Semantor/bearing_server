package org.bitbucket.semantor.repository;

import org.bitbucket.semantor.data.object.Order;
import org.bitbucket.semantor.data.object.OrderStatus;
import org.bitbucket.semantor.data.object.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long>, JpaSpecificationExecutor<Order> {

    @Override
    @NotNull
    List<Order> findAll();

    @Override
    @NotNull
    Page<Order> findAll(@NotNull Pageable pageable);

    @Override
    @NotNull
    List<Order> findAll(@NotNull Sort sort);

    @Override
    @NotNull
    List<Order> findAllById(@NotNull Iterable<Long> longs);

    @Override
    @NotNull
    Optional<Order> findById(@NotNull Long aLong);

    @Override
    <S extends Order> @NotNull S save(@NotNull S entity);

    /**
     * Return list of orders for a given user
     * return empty list if problem occurred
     *
     * @return list of orders
     */
    List<Order> findByUser(User user);

    /**
     * Return list of orders for a given status
     * return empty list if problem occurred
     *
     * @return list of orders
     */
    List<Order> findByOrderStatus(OrderStatus status);

    /**
     * Return list of orders for a given status and user
     * return empty list if problem occurred
     *
     */
    List<Order> findByOrderStatusAndUser(OrderStatus status, User user);

    @Override
    @NotNull
    Optional<Order> findOne(Specification<Order> spec);

    @Override
    @NotNull
    List<Order> findAll(Specification<Order> spec);

    @Override
    @NotNull
    Page<Order> findAll(Specification<Order> spec, @NotNull Pageable pageable);

    @Override
    @NotNull
    List<Order> findAll(Specification<Order> spec, @NotNull Sort sort);

    @Override
    long count(Specification<Order> spec);
}
