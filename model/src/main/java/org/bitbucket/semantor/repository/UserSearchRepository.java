package org.bitbucket.semantor.repository;

import org.bitbucket.semantor.data.object.UserSearch;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;


public interface UserSearchRepository extends JpaRepository<UserSearch, Long>, JpaSpecificationExecutor<UserSearch> {

    @Override
    @NotNull
    List<UserSearch> findAll();

    @Override
    @NotNull
    List<UserSearch> findAll(@NotNull Sort sort);

    @Override
    @NotNull
    Page<UserSearch> findAll(@NotNull Pageable pageable);

    @Override
    <S extends UserSearch> @NotNull S save(@NotNull S entity);

    @Override
    @NotNull
    Optional<UserSearch> findById(@NotNull Long aLong);

    @Override
    <S extends UserSearch> @NotNull Page<S> findAll(@NotNull Example<S> example, @NotNull Pageable pageable);

    @Override
    @NotNull
    Optional<UserSearch> findOne(Specification<UserSearch> spec);

    @Override
    @NotNull
    List<UserSearch> findAll(Specification<UserSearch> spec);

    @Override
    @NotNull
    Page<UserSearch> findAll(Specification<UserSearch> spec, @NotNull Pageable pageable);

    @Override
    @NotNull
    List<UserSearch> findAll(Specification<UserSearch> spec, @NotNull Sort sort);

    @Override
    long count(Specification<UserSearch> spec);
}
