package org.bitbucket.semantor.repository.specs;

import org.bitbucket.semantor.data.object.UserSearch;
import org.bitbucket.semantor.data.object.UserSearch_;
import org.bitbucket.semantor.data.object.User_;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.domain.Specification;

import javax.validation.constraints.Null;
import java.util.Date;
import java.util.UUID;

public class UserSearchSpecs {

    public Specification<UserSearch> search(@Nullable UUID userID,
                                            @Nullable Date from,
                                            @Nullable Date to,
                                            @Nullable String search){
        Specification<UserSearch> result = Specification.where(null);
        if (userID!=null)
            result.and(getByUUID(userID));

        if (from!=null)
            result.and(getByDateFrom(from));

        if (to!=null)
            result.and(getByDateTo(to));

        if (search!=null)
            result.and(getBySearch(wrap(search)));

        return result;
    }

    private Specification<UserSearch> getBySearch(String searchLike) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(UserSearch_.search),searchLike);
    }

    private Specification<UserSearch> getByDateFrom(Date from) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get(UserSearch_.date),from);
    }

    private Specification<UserSearch> getByDateTo(Date to) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(UserSearch_.date),to);
    }

    private Specification<UserSearch> getByUUID(UUID userID) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(UserSearch_.user).get(User_.uuid) , userID);
    }



    private String wrap(String source){
        return String.format("%%%s%%",source);
    }

}
