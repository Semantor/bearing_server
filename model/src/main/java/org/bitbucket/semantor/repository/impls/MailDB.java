package org.bitbucket.semantor.repository.impls;


import lombok.extern.slf4j.Slf4j;
import org.bitbucket.semantor.data.object.Mail;
import org.bitbucket.semantor.repository.MailRepository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class MailDB implements MailRepository {
    private final String PATH_TO_FILE;
    private List<Mail> data;

    public MailDB(String pathToFile) {
        PATH_TO_FILE = pathToFile;
        data = new ArrayList<>();
        loadInformation();
    }

    private void loadInformation() {
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(PATH_TO_FILE)))) {
            this.data = (List<Mail>) ois.readObject();
        } catch (IOException ex) {
            log.error("IOException during read file");
        } catch (ClassNotFoundException ex) {
            log.error("File is empty");
        }
    }

    public void saveInformation() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(PATH_TO_FILE)))) {
            oos.writeObject(data);
            System.out.println("Writing is complete");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<Mail> getData() {
        return data;
    }

    public void addMail(Mail mail) {
        data.add(mail);
    }
}
