package org.bitbucket.semantor.repository;

import org.bitbucket.semantor.data.object.BuyingPosition;
import org.bitbucket.semantor.data.object.ItemType;
import org.bitbucket.semantor.data.object.Order;
import org.bitbucket.semantor.data.object.catalog.Bearing;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BuyingPositionRepository extends JpaRepository<BuyingPosition, Long> {
    List<BuyingPosition> findByOrder(@NotNull Order order);

    List<BuyingPosition> findByBearing(@NotNull Bearing bearing);

    @Override
    <S extends BuyingPosition> @NotNull List<S> saveAll(@NotNull Iterable<S> entities);

    @Override
    <S extends BuyingPosition> @NotNull S save(@NotNull S entity);
}
