package org.bitbucket.semantor.repository.impls;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.bitbucket.semantor.repository.ImageRepository;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class LocalHashedImageRepositoryImpl implements ImageRepository {

    private final String pathToImages;
    private final File folder;
    private final Map<String, File> map;

    public LocalHashedImageRepositoryImpl(String pathToImages) {
        this.pathToImages = pathToImages;
        folder = new File(pathToImages);
        map = new HashMap<>();
    }

    @Override
    public byte[] getImages(String match) {
        byte[] bytes = new byte[0];
        if (isExist(match)) {
            File file = map.get(match);
            try (FileInputStream in = new FileInputStream(file)) {
                bytes = IOUtils.toByteArray(in);
            } catch (IOException ex) {
                System.out.println(pathToImages + "/" + file);
                throw new IllegalStateException("wrong path to images");
            }
        }
        return bytes;
    }

    @Override
    public boolean isExist(String match) {
        log.info("find with match: " + match);
        if (map.containsKey(match)) {
            if (map.get(match).exists()) return true;
            else map.remove(match);
        }
        if (!folder.exists()
                || !folder.isDirectory()
                || folder.listFiles().length == 0) {
            return false;
        }
        for (File f :
                folder.listFiles()) {
            if (f.getName().toLowerCase().contains(match.toLowerCase())) {
                map.put(match, f);
                return true;
            }
        }
        return false;
    }
}
