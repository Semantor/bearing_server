package org.bitbucket.semantor.repository.impls;


import lombok.extern.slf4j.Slf4j;
import org.bitbucket.semantor.data.object.Order;
import org.bitbucket.semantor.repository.OrderRepository;
import org.bitbucket.semantor.data.object.OrderStatus;
import org.bitbucket.semantor.data.object.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
public class OrderDB implements OrderRepository {
    private final String FILE_NAME;
    private List<Order> data;

    public OrderDB(String pathToFile){
        FILE_NAME=pathToFile;
        data = new ArrayList<>();
        loadInformation();
    }

    private void loadInformation() {
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(FILE_NAME)))) {
            this.data = (List<Order>) ois.readObject();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void saveInformation() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(FILE_NAME)))) {

            oos.writeObject(data);
            System.out.println("Writing is complete");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<Order> findAll() {
        return data;
    }

    @Override
    public Order save(Order order) {
        data.add(order);
        return order;
    }

    @Override
    public List<Order> findByUser(User user) {
        List<Order> orders = new ArrayList<>();
        for (Order o :
                data) {
            if (o.getUser().equals(user)) orders.add(o);
        }
        return orders;
    }



    @Override
    public Optional<Order> updateOrder(Order order) {
        log.error("Not implemented yet");
        throw new IllegalStateException("Not implemented yet");
    }

    @Override
    public Optional<Order> findById(long id) {
        log.error("Not implemented yet");
        throw new IllegalStateException("Not implemented yet");
    }

    @Override
    public List<Order> findByOrderStatus(OrderStatus status) {
        log.error("Not implemented yet");
        throw new IllegalStateException("Not implemented yet");
    }

    @Override
    public List<Order> findByOrderStatusAndUser(OrderStatus status, User user) {
        log.error("Not implemented yet");
        throw new IllegalStateException("Not implemented yet");
    }
}
