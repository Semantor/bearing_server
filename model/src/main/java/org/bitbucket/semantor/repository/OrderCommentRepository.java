package org.bitbucket.semantor.repository;

import org.bitbucket.semantor.data.object.OrderComment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderCommentRepository extends JpaRepository<OrderComment, Long> {
}
