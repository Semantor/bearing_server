package org.bitbucket.semantor.repository;

public interface ImageRepository {

    /**
     * Return array of bytes of target images.
     * Return empty {@code byte[0]} if no one exist.
     *
     * @param match
     * @return {@code byte[]} of images
     */
    byte[] getImages(String match);

    /**
     * Return true if any file with this matching are exist.
     *
     * @param match
     * @return true if exist
     */
    boolean isExist(String match);
}
