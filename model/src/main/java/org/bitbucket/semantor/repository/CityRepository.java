package org.bitbucket.semantor.repository;

import org.bitbucket.semantor.data.object.City;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface CityRepository extends JpaRepository<City, Long>, JpaSpecificationExecutor<City> {
    @Override
    <S extends City> @NotNull S save(@NotNull S entity);

    @Override
    @NotNull
    List<City> findAll();

    @Override
    @NotNull
    Optional<City> findById(@NotNull Long aLong);

    Optional<City> findByName(String name);

    @Override
    @NotNull
    Optional<City> findOne(Specification<City> spec);

    @Override
    @NotNull
    List<City> findAll(Specification<City> spec);

    @Override
    @NotNull
    Page<City> findAll(Specification<City> spec, @NotNull Pageable pageable);

    @Override
    @NotNull
    List<City> findAll(Specification<City> spec, @NotNull Sort sort);

    @Override
    long count(Specification<City> spec);
}
