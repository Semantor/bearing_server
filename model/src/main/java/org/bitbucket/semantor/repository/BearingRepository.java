package org.bitbucket.semantor.repository;

import org.bitbucket.semantor.data.object.catalog.Bearing;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

/**
 * Interface to find and get bearings from database;
 */
public interface BearingRepository extends JpaRepository<Bearing, Integer>, JpaSpecificationExecutor<Bearing> {

    @Override
    List<Bearing> findAll();

    List<Bearing> findByName(String name);

    List<Bearing> findByAlias(String alias);

    @Override
    List<Bearing> findAll(Sort sort);

    @Override
    Optional<Bearing> findOne(Specification<Bearing> spec);

    @Override
    List<Bearing> findAll(Specification<Bearing> spec);

    @Override
    Page<Bearing> findAll(Specification<Bearing> spec, Pageable pageable);

    @Override
    List<Bearing> findAll(Specification<Bearing> spec, Sort sort);

    @Override
    long count(Specification<Bearing> spec);

    @Override
    <S extends Bearing> S save(S entity);

    @Override
    Page<Bearing> findAll(Pageable pageable);

    @Override
    Optional<Bearing> findById(Integer integer);
}
