package org.bitbucket.semantor.repository;

import org.bitbucket.semantor.data.object.Mail;

import java.util.List;

public interface MailRepository {
    List<Mail> getData();
}
