package org.bitbucket.semantor.repository;

import org.bitbucket.semantor.data.object.InfoCard;

import java.util.Date;
import java.util.List;

public interface InfoCardRepository {

    void save(InfoCard infoCard);

    List<InfoCard> findByDate(Date current);
}