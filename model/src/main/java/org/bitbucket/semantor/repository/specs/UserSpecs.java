package org.bitbucket.semantor.repository.specs;

import org.bitbucket.semantor.data.object.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.domain.Specification;

import javax.validation.constraints.Null;
import java.sql.Date;
import java.util.List;
import java.util.UUID;

public class UserSpecs {


    public Specification<User> search(@Nullable List<UUID> ids,
                                      @Nullable UUID userID,
                                      @Nullable String nameLike,
                                      @Nullable String phoneLike,
                                      @Nullable String emailLike,
                                      @Nullable Role role,
                                      @Nullable String surnameLike,
                                      @Nullable String cityLike,
                                      @Nullable City city,
                                      @Nullable Date registerFrom, Date registerTo
    ) {
        Specification<User> specification = Specification.where(null);
        if (ids != null)
            specification.and(getUserByIds(ids));
        if (userID!=null)
            specification.and(getUserById(userID));
        if (nameLike != null)
            specification.and(getUserByName(wrap(nameLike)));
        if (phoneLike != null)
            specification.and(getUserByPhone(wrap(phoneLike)));
        if (emailLike != null)
            specification.and(getUserByEmail(wrap(emailLike)));
        if (role != null)
            specification.and(getUserByRole(role));
        if (surnameLike != null)
            specification.and(getUserBySurname(wrap(surnameLike)));
        if (cityLike != null)
            specification.and(getUserByCity(wrap(cityLike)));
        if (city != null)
            specification.and(getUserByCity(city));
        if (registerFrom != null)
            specification.and(getUserByCreatedDateFrom(registerFrom));
        if (registerTo != null)
            specification.and(getUserByCreatedDateTo(registerTo));
        return specification;
    }

    private String wrap(String source){
        return String.format("%%%s%%",source);
    }

    private Specification<User> getUserById(@NotNull UUID userID) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(User_.uuid),userID);
    }

    public Specification<User> getUserByIds(List<UUID> ids) {
        return (root, query, criteriaBuilder) -> root.get(User_.uuid).in(ids);
    }



    public Specification<User> getUserByName(String name) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(User_.name), name);
    }

    public Specification<User> getUserByPhone(String phone) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(User_.phone), phone);
    }

    public Specification<User> getUserByEmail(String email) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(User_.email), email);
    }

    public Specification<User> getUserByRole(Role role) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(User_.role), role);
    }

    public Specification<User> getUserBySurname(String surname) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(User_.surname), surname);
    }

    public Specification<User> getUserByCity(String city) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(User_.city).get(City_.name), city);
    }

    public Specification<User> getUserByCity(City city) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(User_.city), city);
    }

    public Specification<User> getUserByCreatedDate(Date from, Date to) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.between(root.get(User_.created), from, to);
    }

    public Specification<User> getUserByCreatedDateFrom(Date from) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.lessThan(root.get(User_.created), from);
    }

    public Specification<User> getUserByCreatedDateTo(Date to) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThan(root.get(User_.created), to);
    }
}
