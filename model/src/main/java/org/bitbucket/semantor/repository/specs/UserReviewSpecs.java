package org.bitbucket.semantor.repository.specs;

import org.bitbucket.semantor.data.object.UserReview;
import org.bitbucket.semantor.data.object.UserReview_;
import org.bitbucket.semantor.data.object.User_;
import org.bitbucket.semantor.data.object.catalog.Bearing;
import org.bitbucket.semantor.data.object.catalog.Bearing_;
import org.springframework.data.jpa.domain.Specification;

import java.util.Date;
import java.util.UUID;

public class UserReviewSpecs {
    public Specification<UserReview> search(UUID userid,
                                            Integer bearingId,
                                            Date from, Date to){
        Specification<UserReview> spec = Specification.where(null);

        if (userid!=null)
            spec.and(getByUserID(userid));
        if (bearingId!=null)
            spec.and(getByBearingId(bearingId));
        if (from!=null)
            spec.and(getByDateFrom(from));
        if (from!=null)
            spec.and(getByDateTo(to));


        return spec;
    }

    public Specification<UserReview> getByDateTo(Date to) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(UserReview_.date),to);
    }

    public Specification<UserReview> getByDateFrom(Date from) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get(UserReview_.date),from);
    }

    public Specification<UserReview> getByBearingId(Integer bearingId) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(UserReview_.item).get(Bearing_.id),bearingId);
    }

    public Specification<UserReview> getByUserID(UUID userid) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(UserReview_.user).get(User_.uuid),userid);
    }
}
