package org.bitbucket.semantor.repository.impls;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.bitbucket.semantor.data.object.catalog.Bearing;
import org.bitbucket.semantor.repository.BearingRepository;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
public class CSVconnector implements BearingRepository {
    private Workbook wb;
    private List<Bearing> bearings = new ArrayList<>();
    private final String FILE_NAME;

    public CSVconnector(String pathToPrice) {
        this.FILE_NAME = pathToPrice;
        try {
            loadInformation();
        } catch (IOException ex) {
            System.out.printf("error");
        }
    }

    private void loadInformation() throws IOException {
        FileInputStream fls = new FileInputStream(FILE_NAME);
        wb = new HSSFWorkbook(fls);
        fls.close();
        int k = wb.getSheetAt(0).getLastRowNum();
        wb.getSheetAt(0).getRow(0).getCell(0).getStringCellValue();
        for (int i = 6; i < k; i++) {
            bearings.add(
                    Bearing.builder()
                            .id(bearings.size())
                            .name(getRightType(wb.getSheetAt(0).getRow(i).getCell(1)))
                            .fullname(getRightType(wb.getSheetAt(0).getRow(i).getCell(2)))
                            .price(getRightType(wb.getSheetAt(0).getRow(i).getCell(3)))
                            .amount(getRightType(wb.getSheetAt(0).getRow(i).getCell(7)))
                            .din(getRightType(wb.getSheetAt(0).getRow(i).getCell(8)))
                            .dout(getRightType(wb.getSheetAt(0).getRow(i).getCell(9)))
                            .a(getRightType(wb.getSheetAt(0).getRow(i).getCell(10)))
                            .manufacturer(getRightType(wb.getSheetAt(0).getRow(i).getCell(11)))
                            .description(getRightType(wb.getSheetAt(0).getRow(i).getCell(12)))
                            .alias("")
                            .mark("")
                            .build());
        }

    }

    @Override
    public List<Bearing> findAll() {
        return bearings;
    }

    private static String getRightType(Cell cell) {
        String s = null;
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                s = cell.getStringCellValue();
                break;
            case Cell.CELL_TYPE_NUMERIC:
                s = String.valueOf(cell.getNumericCellValue());
                break;
        }
        return s;
    }

    @Override
    public List<Bearing> findByArgument(String s) {
        List<Bearing> bearings1 = new ArrayList<>();
        for (Bearing b :
                bearings) {
            if (b.getName().contains(s) ||
                    b.getFullname().contains(s) ||
                    b.getDin().contains(s) ||
                    b.getDout().contains(s) ||
                    b.getA().contains(s) ||
                    b.getManufacturer().contains(s) ||
                    b.getDescription().contains(s)) {
                bearings1.add(b);
            }
        }
        return bearings1;
    }

    @Override
    public List<Bearing> findByName(String s) {
        List<Bearing> bearings1 = new ArrayList<>();
        for (Bearing b :
                bearings) {
            if (b.getName().toLowerCase().contains(s.toLowerCase())) bearings1.add(b);
        }
        return bearings1;
    }

    @Override
    public List<Bearing> findByAlias(String alias) {
        List<Bearing> bearings1 = new ArrayList<>();
        for (Bearing b :
                bearings) {
            if (b.getAlias() != null && b.getAlias().toLowerCase().contains(alias.toLowerCase())) bearings1.add(b);
        }
        return bearings1;
    }

    @Override
    public List<Bearing> findByMark(String mark) {
        log.error("Not implemented yet");
        throw new IllegalStateException("Not implemented yet");
    }

    @Override
    public Optional<Bearing> findById(int id) {
        Optional<Bearing> bearing = Optional.of(bearings.get(id));
        return bearing;
    }

    @Override
    public List<Bearing> findByDimensional(double a, double b, double c) {
        log.error("Not implemented yet");
        throw new IllegalStateException("Not implemented yet");
    }
}
