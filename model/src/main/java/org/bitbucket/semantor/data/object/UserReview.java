package org.bitbucket.semantor.data.object;

import lombok.*;
import org.bitbucket.semantor.data.object.catalog.Bearing;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
@Setter(AccessLevel.PRIVATE)
public class UserReview {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @JoinColumn @ManyToOne
    private User user;
    @JoinColumn @ManyToOne
    private Bearing bearing;
    private Timestamp date = new Timestamp(System.currentTimeMillis());

    @Builder
    private UserReview(User user, Bearing bearing) {
        this.user = user;
        this.bearing = bearing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        UserReview that = (UserReview) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return 180863332;
    }
}
