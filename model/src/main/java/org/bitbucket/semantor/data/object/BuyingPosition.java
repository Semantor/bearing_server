package org.bitbucket.semantor.data.object;

import lombok.*;
import org.bitbucket.semantor.data.object.catalog.Bearing;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Getter
@ToString
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Setter(AccessLevel.PACKAGE)
public class BuyingPosition {
    @Id
    private long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private Order order;
    @ManyToOne
    @JoinColumn
    private Bearing bearing;
    private int count;
    private double price;

    @Builder
    public BuyingPosition(Order order, Bearing bearing, int count, double price) {
        this.order = order;
        this.bearing = bearing;
        this.count = count;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BuyingPosition that = (BuyingPosition) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return 152062512;
    }
}
