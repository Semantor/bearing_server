package org.bitbucket.semantor.data.object;

import org.bitbucket.semantor.data.object.catalog.Bearing;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserReview.class)
public abstract class UserReview_ {
    public static volatile SingularAttribute<UserReview, Long> id;
    public static volatile SingularAttribute<UserReview, User> user;
    public static volatile SingularAttribute<UserReview, Bearing> item;
    public static volatile SingularAttribute<UserReview, Date> date;

    public static final String ID = "id";
    public static final String USER = "user";
    public static final String ITEM = "item";
    public static final String DATE = "date";
}
