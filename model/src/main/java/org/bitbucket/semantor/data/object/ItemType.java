package org.bitbucket.semantor.data.object;

public enum ItemType {
    BEARING,
    BELT,
    AUTOPART
}
