package org.bitbucket.semantor.data.object.catalog;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

@Getter
@ToString
@Builder
@Entity
@Setter(AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Bearing implements Item {
    @Id
    private int id;
    /**
     * main name
     */
    private String name;
    /**
     * full name
     */
    private String fullname;
    /**
     * main price for client
     */
    private double price;
    /**
     * estimated amount
     */
    private int amount;
    /**
     * inner diameter
     */
    private double din;
    /**
     * external diameter
     */
    private double dout;
    /**
     * depth
     */
    private double a;
    /**
     * manufacturer with own alias
     */
    private String manufacturer;
    /**
     * description
     */
    private String description;
    /**
     * list of alias comma separated
     */
    private String alias;
    /**
     * list of mark comma separated
     */
    private String mark;

    @Transient
    public static final Bearing emptyBearing = new Bearing(-1,"","",0.0,0,
            0.0,0.0,0.0,"","","","");
}
