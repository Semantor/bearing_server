package org.bitbucket.semantor.data.object;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;


@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Setter(AccessLevel.PRIVATE)
@Getter
@ToString
public class UserActivity {
    @Id
    @Column(name = "user_activity_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    private User user;
    private ActivityType activityType;
    @Column(name = "created_at")
    private final Timestamp createdAt = new Timestamp(System.currentTimeMillis());
    private String item;
    private ItemType itemType;
    private String option;


    @Builder
    public UserActivity(User user, ActivityType activityType, String item, ItemType itemType, String option) {
        this.user = user;
        this.activityType = activityType;
        this.item = item;
        this.itemType = itemType;
        this.option = option;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        UserActivity that = (UserActivity) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return 1850720104;
    }
}
