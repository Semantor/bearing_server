package org.bitbucket.semantor.data.object;


import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

@Table(name = "users")
@Getter
@Setter(AccessLevel.PACKAGE)
@ToString
@Entity
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class User {
    @Id
    @Setter(AccessLevel.PRIVATE)
    @Builder.Default
    private UUID uuid = UUID.randomUUID();
    @Size(min = 10, max = 10, message = "Size must be exactly 10 numbers")
    @Pattern(regexp = "9\\d{9}$", message = "string like 9[0123456789]{9}$")
    @Column(length = 10)
    private String phone;
    @Builder.Default
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private Role role = Role.USER;
    @Builder.Default
    private String email = "";
    @Size(min = 4, message = "minimum 4 symbols")
    private String name;
    @Builder.Default
    private String surname = "";
    @Builder.Default
    @ManyToOne
    private City city = City.DEFAULT_CITY;
    @Column(name = "created_at")
    @Setter(AccessLevel.PRIVATE)
    private Timestamp createdAt = new Timestamp(System.currentTimeMillis());
    private String comment;

    @Builder
    public User(String phone, Role role, String email, String name, String surname, City city, String comment) {
        this.phone = phone;
        this.role = role;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.comment = comment;
    }

    private User() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        User user = (User) o;

        return Objects.equals(uuid, user.uuid);
    }

    @Override
    public int hashCode() {
        return 562048007;
    }
}
