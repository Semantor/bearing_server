package org.bitbucket.semantor.data.object;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.sql.Timestamp;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserActivity.class)
public abstract class UserActivity_ {
    public static volatile SingularAttribute<UserActivity, Long> id;
    public static volatile SingularAttribute<UserActivity, User> user;
    public static volatile SingularAttribute<UserActivity, ActivityType> activityType;
    public static volatile SingularAttribute<UserActivity, Timestamp> createdAt;
    public static volatile SingularAttribute<UserActivity, String> item;
    public static volatile SingularAttribute<UserActivity, ItemType> itemType;
    public static volatile SingularAttribute<UserActivity, String> option;

    public static final String ID = "id";
    public static final String USER = "user";
    public static final String ACTIVITY_TYPE = "activityType";
    public static final String CREATED_AT = "createdAt";
    public static final String ITEM = "item";
    public static final String ITEM_TYPE = "itemType";
    public static final String OPTION = "option";
}
