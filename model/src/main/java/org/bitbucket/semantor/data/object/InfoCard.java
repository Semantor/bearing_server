package org.bitbucket.semantor.data.object;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder
@Data
public class InfoCard {

    private final String label;
    private final String imageName;
    private final String text;
    private final Date from;
    private final Date to;
}
