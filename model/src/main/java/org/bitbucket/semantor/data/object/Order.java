package org.bitbucket.semantor.data.object;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@ToString
@Entity
@Table(name = "orders")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Setter(AccessLevel.PACKAGE)
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @JoinColumn
    @ManyToOne
    private User user;
    @OneToMany(targetEntity = BuyingPosition.class, fetch = FetchType.EAGER)
    @Builder.Default private List<BuyingPosition> positions = new ArrayList<>();
    @Setter(AccessLevel.PUBLIC)
    private OrderStatus orderStatus;
    @OneToMany(targetEntity = OrderComment.class, fetch = FetchType.LAZY)
    @ToString.Exclude
    @Builder.Default private List<OrderComment> comments = new ArrayList<>(); // comments for/from employee
    private Timestamp createdAt = new Timestamp(System.currentTimeMillis());

    @Builder
    public Order(User user, List<BuyingPosition> positions, OrderStatus orderStatus, List<OrderComment> comments) {
        this.comments = comments;
        this.user = user;
        this.positions = positions;
        this.orderStatus = orderStatus;
    }

    private Order() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Order order = (Order) o;
        return Objects.equals(id, order.id);
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Transient
    public static final Order DEFAULT_ORDER = new Order(-1, null,
            new ArrayList<>(), OrderStatus.ERROR,
            new ArrayList<>(), new Timestamp(System.currentTimeMillis()));

}
