package org.bitbucket.semantor.data.object;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class Mail {
    private final long id;
    private final Order order;
    private final String theme;
    private final String text;
    private final String status;
    @Builder.Default private final Date created= new Date();
}
