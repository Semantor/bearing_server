package org.bitbucket.semantor.data.object;

public enum ActivityType {
    CREATE_ORDER,
    REVIEW_BEARING,
    SEARCH_BEARING,
    REVIEW_IMAGE,
    LOGIN
}
