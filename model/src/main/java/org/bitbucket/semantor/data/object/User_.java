package org.bitbucket.semantor.data.object;


import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.sql.Timestamp;
import java.util.UUID;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(User.class)
public abstract class User_ {

    public static volatile SingularAttribute<User, UUID> uuid;
    public static volatile SingularAttribute<User, String> phone;
    public static volatile SingularAttribute<User, Role> role;
    public static volatile SingularAttribute<User, String> email;
    public static volatile SingularAttribute<User, String> name;
    public static volatile SingularAttribute<User, String> surname;
    public static volatile SingularAttribute<User, City> city;
    public static volatile SingularAttribute<User, Timestamp> created;


    public static final String UUID = "uuid";
    public static final String PHONE = "phone";
    public static final String ROLE = "role";
    public static final String EMAIL = "email";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String CITY = "city";
    public static final String CREATED_AT = "created_at";
}

