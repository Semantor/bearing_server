package org.bitbucket.semantor.data.object.catalog;

import org.bitbucket.semantor.data.object.User;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Bearing.class)
public abstract class Bearing_ {

    public static volatile SingularAttribute<Bearing, Integer> id;
    public static volatile SingularAttribute<Bearing, String> name;
    public static volatile SingularAttribute<Bearing, String> fullname;
    public static volatile SingularAttribute<Bearing, Double> price;
    public static volatile SingularAttribute<Bearing, String> amount;
    public static volatile SingularAttribute<Bearing, Double> din;
    public static volatile SingularAttribute<Bearing, Double> dout;
    public static volatile SingularAttribute<Bearing, Double> a;
    public static volatile SingularAttribute<Bearing, String> manufacturer;
    public static volatile SingularAttribute<Bearing, String> description;
    public static volatile SingularAttribute<Bearing, String> alias;
    public static volatile SingularAttribute<Bearing, String> mark;

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String FULLNAME = "fullname";
    public static final String PRICE = "price";
    public static final String AMOUNT = "amount";
    public static final String DIN = "din";
    public static final String DOUT = "dout";
    public static final String A = "a";
    public static final String MANUFACTURER = "manufacturer";
    public static final String DESCRIPTION = "description";
    public static final String ALIAS = "alias";
    public static final String MARK = "mark";
}
