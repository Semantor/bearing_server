package org.bitbucket.semantor.data.object;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Builder
@Getter
@ToString
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Setter(AccessLevel.PRIVATE)
public class UserSearch {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.PRIVATE)
    private long id;
    @ManyToOne @JoinColumn
    private User user;
    private String search;
    @Builder.Default
    private Date date = new Timestamp(System.currentTimeMillis());

    @Builder
    public UserSearch(User user, String search) {
        this.user = user;
        this.search = search;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        UserSearch that = (UserSearch) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return 755578854;
    }
}
