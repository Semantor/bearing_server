package org.bitbucket.semantor.data.object;

public enum OrderStatus {

    ERROR,
    CHECKING,
    RESERVED,
    CANCELED,
    SOLD_OUT

}
