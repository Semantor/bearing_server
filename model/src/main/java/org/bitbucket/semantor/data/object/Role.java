package org.bitbucket.semantor.data.object;

import lombok.AllArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public enum Role {
    GUEST(false,false,false,
            false,false,false,
            false,false,
            false,false,false,
            false,false,false,
            false,false),
    USER(true,true,false,
            false,false,false,
            true,false,
            true,false,false,
            false,false,false,
            true,false),
    ADMIN(false,false,false,
            false,true,true,
            false,true,
            false,true,true,
            true,true,true,
            false,true),
    MANAGER(false,false,false,
            false,true,true,
            false,true,
            false,true,true,
            false,true,true,
            false,true),
    WATCHER(false,false,false,
            false,true,false,
            false,false,
            false,false,false,
            false,true,true,
            false,true);

    @Id@GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    public final boolean canCreateOwnOrder;
    public final boolean canReadOwnOrder;
    public final boolean canChangeOwnOrder;

    public final boolean canCreateElseOrder;
    public final boolean canReadElseOrder;
    public final boolean canChangeElseOrder;

    public final boolean canAddChangeOrdersUsersComment;
    public final boolean canAddChangeOrdersProcessingComment;

    public final boolean canChangeOwnProfile;
    public final boolean canChangeElseProfile;

    public final boolean canAddChangeProfilesComment;
    public final boolean canChangeUsersRole;

    public final boolean canReadUserReview;
    public final boolean canReadUserSearch;

    public final boolean canReadOwnOrderList;
    public final boolean canReadElseOrderList;


    Role(boolean canCreateOwnOrder, boolean canReadOwnOrder, boolean canChangeOwnOrder, boolean canCreateElseOrder, boolean canReadElseOrder, boolean canChangeElseOrder, boolean canAddChangeOrdersUsersComment, boolean canAddChangeOrdersProcessingComment, boolean canChangeOwnProfile, boolean canChangeElseProfile, boolean canAddChangeProfilesComment, boolean canChangeUsersRole, boolean canReadUserReview, boolean canReadUserSearch, boolean canReadOwnOrderList, boolean canReadElseOrderList) {
        this.canCreateOwnOrder = canCreateOwnOrder;
        this.canReadOwnOrder = canReadOwnOrder;
        this.canChangeOwnOrder = canChangeOwnOrder;
        this.canCreateElseOrder = canCreateElseOrder;
        this.canReadElseOrder = canReadElseOrder;
        this.canChangeElseOrder = canChangeElseOrder;
        this.canAddChangeOrdersUsersComment = canAddChangeOrdersUsersComment;
        this.canAddChangeOrdersProcessingComment = canAddChangeOrdersProcessingComment;
        this.canChangeOwnProfile = canChangeOwnProfile;
        this.canChangeElseProfile = canChangeElseProfile;
        this.canAddChangeProfilesComment = canAddChangeProfilesComment;
        this.canChangeUsersRole = canChangeUsersRole;
        this.canReadUserReview = canReadUserReview;
        this.canReadUserSearch = canReadUserSearch;
        this.canReadOwnOrderList = canReadOwnOrderList;
        this.canReadElseOrderList = canReadElseOrderList;
    }
}
