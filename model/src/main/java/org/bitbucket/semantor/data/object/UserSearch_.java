package org.bitbucket.semantor.data.object;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.sql.Timestamp;
import java.util.Date;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserSearch.class)
public abstract class UserSearch_ {
    public static volatile SingularAttribute<UserSearch, Long> id;
    public static volatile SingularAttribute<UserSearch, User> user;
    public static volatile SingularAttribute<UserSearch, String> search;
    public static volatile SingularAttribute<UserSearch, Timestamp> date;

    public static final String ID = "id";
    public static final String USER = "user";
    public static final String SEARCH = "search";
    public static final String DATE = "date";
}
