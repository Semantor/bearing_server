package org.bitbucket.semantor.data.object;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Getter
@ToString
@Entity
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Setter(AccessLevel.NONE)
public class City {
    @Id @GeneratedValue(strategy = GenerationType.AUTO) private long id;
    private String name;

    @Transient
    public static final City DEFAULT_CITY = new City(1, "Unknown");

    @Builder
    public City(String name) {
        this.name = name;
    }

    private City() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        City city = (City) o;
        return Objects.equals(id, city.id);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
