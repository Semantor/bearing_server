package org.bitbucket.semantor.dto;

import lombok.Builder;
import lombok.Data;
import org.bitbucket.semantor.data.object.BuyingPosition;
import org.bitbucket.semantor.data.object.OrderComment;
import org.bitbucket.semantor.data.object.OrderStatus;

import java.util.List;

@Data
@Builder
public class OrderDTO {
    private final long id;
    private final UserDTO user;
    List<BuyingPosition> buyingPositions;
    private final OrderStatus status;
    private final List <OrderComment> comments;
}
