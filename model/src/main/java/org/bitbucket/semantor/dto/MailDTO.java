package org.bitbucket.semantor.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MailDTO {
    private final int id;
    private final OrderDTO orderDTO;
    private final String theme;
    private final String text;
    private final String status;
}
