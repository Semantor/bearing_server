package org.bitbucket.semantor.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BearingDTO {
    private final int id;
    private final String name;
    private final String fullname;
    private final Double price;
    private final int amount;
    private final Double din;
    private final Double dout;
    private final Double a;
    private final String manufacturer;
    private final String description;
    private final String alias;
    private final String mark;
}
