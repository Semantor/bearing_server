package org.bitbucket.semantor.dto;

import lombok.Builder;
import lombok.Data;
import org.bitbucket.semantor.data.object.City;

@Data
@Builder
public class UserDTO {
    private final String email;
    private final String name;
    private final String surname;
    private final String phone;
    private final City city;

    public static final UserDTO DEFAULT_USER = UserDTO.builder().email("").surname("").name("").phone("").build();
}
