package org.bitbucket.semantor.resources;

import org.bitbucket.semantor.data.object.catalog.Bearing;
import org.jetbrains.annotations.NotNull;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

public class BearingResourceAssembler extends RepresentationModelAssemblerSupport<Bearing, BearingResource> {

    public BearingResourceAssembler(Class<?> controllerClass, Class<BearingResource> resourceType) {
        super(controllerClass, resourceType);
    }

    @Override
    protected @NotNull BearingResource instantiateModel(@NotNull Bearing entity) {
        return new BearingResource(entity);
    }

    @Override
    public @NotNull BearingResource toModel(@NotNull Bearing entity) {
        return createModelWithId(entity.getId(), entity);
    }
}
