package org.bitbucket.semantor.resources;

import org.bitbucket.semantor.data.object.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

public class UserResourceAssembler extends RepresentationModelAssemblerSupport<User,UserResource> {
    public UserResourceAssembler(Class<?> controllerClass, Class<UserResource> resourceType) {
        super(controllerClass, resourceType);
    }

    @Override
    protected @NotNull UserResource instantiateModel(@NotNull User entity) {
        return new UserResource(entity);
    }

    @Override
    public @NotNull UserResource toModel(@NotNull User entity) {
       return createModelWithId(entity.getUuid(),entity);
    }
}
