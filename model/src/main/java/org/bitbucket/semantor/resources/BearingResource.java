package org.bitbucket.semantor.resources;

import lombok.Builder;
import lombok.Getter;
import org.bitbucket.semantor.data.object.catalog.Bearing;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Relation(value = "bearing",collectionRelation = "bearings")
public class BearingResource extends RepresentationModel<BearingResource> {
    private final String name;
    private final String fullname;
    private final double price;
    private final int amount;
    private final double din;
    private final double dout;
    private final double a;
    private final String manufacturer;
    private final String description;
    private final String alias;
    private final String mark;

    public BearingResource(@NotNull Bearing bearing) {
        this.name = bearing.getName();
        this.fullname = bearing.getFullname();
        this.price = bearing.getPrice();
        this.amount = bearing.getAmount();
        this.din = bearing.getDin();
        this.dout = bearing.getDout();
        this.a = bearing.getA();
        this.manufacturer = bearing.getManufacturer();
        this.description = bearing.getDescription();
        this.alias = bearing.getAlias();
        this.mark = bearing.getMark();
    }
}
