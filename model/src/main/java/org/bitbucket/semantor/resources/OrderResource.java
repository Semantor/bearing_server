package org.bitbucket.semantor.resources;

import org.bitbucket.semantor.data.object.BuyingPosition;
import org.bitbucket.semantor.data.object.Order;
import org.bitbucket.semantor.data.object.OrderStatus;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import java.sql.Timestamp;
import java.util.List;

@Relation(value = "order",collectionRelation = "orders")
public class OrderResource extends RepresentationModel<OrderResource> {
    private final UserResource userResource;
    private final List<BuyingPosition> positions;
    private final OrderStatus orderStatus;
    private final Timestamp createdAt;

    public OrderResource(Order order, UserResource userResource) {
        this.userResource = userResource;
        this.positions = order.getPositions();
        this.orderStatus = order.getOrderStatus();
        this.createdAt = order.getCreatedAt();
    }
}
