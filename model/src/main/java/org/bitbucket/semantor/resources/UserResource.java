package org.bitbucket.semantor.resources;

import org.bitbucket.semantor.data.object.City;
import org.bitbucket.semantor.data.object.User;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import java.sql.Timestamp;
import java.util.UUID;

@Relation(value = "user",collectionRelation = "users")
public class UserResource extends RepresentationModel<UserResource> {

    private final UUID uuid;
    private final String phone;
    private final String email;
    private final String name;
    private final String surname;
    private final City city;
    private final Timestamp createdAt;

    public UserResource(User user) {
        this.uuid = user.getUuid();
        this.phone = user.getPhone();
        this.email = user.getEmail();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.city = user.getCity();
        this.createdAt = user.getCreatedAt();
    }
}
