package org.bitbucket.semantor.resources;

import org.bitbucket.semantor.data.object.Order;
import org.jetbrains.annotations.NotNull;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

public class OrderResourceAssembler extends RepresentationModelAssemblerSupport<Order, OrderResource> {

    private final UserResourceAssembler userResourceAssembler;

    public OrderResourceAssembler(Class<?> controllerClass, Class<OrderResource> resourceType, UserResourceAssembler userResourceAssembler) {
        super(controllerClass, resourceType);
        this.userResourceAssembler = userResourceAssembler;
    }

    @Override
    public @NotNull OrderResource toModel(@NotNull Order entity) {
        return createModelWithId(entity.getId(), entity);
    }

    @Override
    protected @NotNull OrderResource instantiateModel(@NotNull Order entity) {
        return new OrderResource(entity, userResourceAssembler.toModel(entity.getUser()));
    }
}
