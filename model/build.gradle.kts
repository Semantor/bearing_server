plugins {
    java
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(Libs.jetBrainsAnnotation)
    implementation(Libs.springHateoas)
    implementation(Libs.jakartaValidation)
    implementation(Libs.poi)
    implementation(Libs.commonIO)
    implementation(Libs.slf4j)
    implementation(Libs.springJpa)
    compileOnly(Libs.lombok)
    testCompileOnly(Libs.lombok)
    annotationProcessor(Libs.lombok)
    testAnnotationProcessor(Libs.lombok)
    testImplementation(Libs.junit)
}
