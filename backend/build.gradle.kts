plugins {
    java
}

repositories {
    mavenCentral()
}
dependencies{
    implementation(Libs.jetBrainsAnnotation)
    implementation(Libs.springJpa)
    implementation(project(":model"))
    implementation(Libs.slf4j)
    compileOnly(Libs.lombok)
    testCompileOnly(Libs.lombok)
    annotationProcessor(Libs.lombok)
    testAnnotationProcessor(Libs.lombok)
}
