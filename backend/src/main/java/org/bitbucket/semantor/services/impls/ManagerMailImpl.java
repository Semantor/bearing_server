package org.bitbucket.semantor.services.impls;

import lombok.AllArgsConstructor;
import org.bitbucket.semantor.dto.MailDTO;
import org.bitbucket.semantor.exchanger.MailExchanger;
import org.bitbucket.semantor.exchanger.OrderExchanger;
import org.bitbucket.semantor.exchanger.UserExchanger;
import org.bitbucket.semantor.repository.MailRepository;
import org.bitbucket.semantor.data.object.Mail;
import org.bitbucket.semantor.services.MailService;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class ManagerMailImpl implements MailService {
    private final MailRepository mailRepository;
    private final UserExchanger userExchanger;
    private final MailExchanger mailExchanger;
    private final OrderExchanger orderExchanger;


    public List<MailDTO> getAllMail() {
        List<MailDTO> mailDTOS = new ArrayList<>();
        for (Mail m :
                mailRepository.getData()) {
            mailDTOS.add(mailExchanger.exchangetoDTO(m));
        }
        return mailDTOS;
    }

    public MailDTO getMailByID(int id) {
        return mailExchanger.exchangetoDTO(mailRepository.getData().get(id));
    }

    @Override
    public List<MailDTO> getAllMailByUser(String user) {
        throw new IllegalStateException("Not implemented yet");
    }
}
