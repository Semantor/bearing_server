package org.bitbucket.semantor.services;

import org.jetbrains.annotations.NotNull;

public interface UserSearchRecorder {
    void addUserSearch(@NotNull String userId,@NotNull String search);
}
