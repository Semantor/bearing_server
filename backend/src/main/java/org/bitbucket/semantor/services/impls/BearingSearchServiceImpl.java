package org.bitbucket.semantor.services.impls;

import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.bitbucket.semantor.dto.BearingDTO;
import org.bitbucket.semantor.exchanger.BearingExchanger;
import org.bitbucket.semantor.repository.BearingRepository;
import org.bitbucket.semantor.repository.specs.BearingSpecs;
import org.bitbucket.semantor.services.BearingSearchService;
import org.bitbucket.semantor.services.UserSearchRecorder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Log
public class BearingSearchServiceImpl implements BearingSearchService {
    private final BearingSpecs bearingSpecs;
    private final BearingRepository bearingRepository;
    private final BearingExchanger bearingExchanger;
    private final UserSearchRecorder userSearchRecorder;

    private static final String START = "User ";
    private static final String BRIDGE = "\nsearch with this parameter:\n";
    private static final String LINE_FEED = "\n";

    @Override
    public List<BearingDTO> search(@NotNull String userid,
                                   String search, Integer bearingId,
                                   String name, String fullname,
                                   String priceMin, String priceMax,
                                   String din, String dinMin, String dinMax,
                                   String dout, String doutMin, String doutMax,
                                   String a, String aMin, String aMax,
                                   String manufacturer, String alias, String description, String mark) {
        if (search != null) userSearchRecorder.addUserSearch(userid, search);

        StringBuilder sb = new StringBuilder(START);

        sb.append(String.format("with UUID:%s.", userid));
        sb.append(BRIDGE);
        if (bearingId != null)
            sb.append(String.format("bearingID:%s, ", bearingId));
        if (name != null)
            sb.append(String.format("name:%s, ", name));
        if (fullname != null)
            sb.append(String.format("fullname:%s.", fullname));
        sb.append(LINE_FEED);
        if (priceMin != null)
            sb.append(String.format("with min price:%s, ", priceMin));
        if (priceMax != null)
            sb.append(String.format("and max price:%s.", priceMax));
        sb.append(LINE_FEED);
        if (din != null)
            sb.append(String.format("din:%s, ", din));
        if (dinMin != null)
            sb.append(String.format("min din:%s, ", dinMin));
        if (dinMax != null)
            sb.append(String.format("max din:%s.", dinMax));
        sb.append(LINE_FEED);
        if (dout != null)
            sb.append(String.format("dout:%s, ", dout));
        if (doutMin != null)
            sb.append(String.format("min dout:%s, ", doutMin));
        if (doutMax != null)
            sb.append(String.format("max dout:%s.", doutMax));
        sb.append(LINE_FEED);
        if (a != null)
            sb.append(String.format("a:%s, ", a));
        if (aMin != null)
            sb.append(String.format("min a:%s, ", aMin));
        if (aMax != null)
            sb.append(String.format("max a:%s.", aMax));
        sb.append(LINE_FEED);
        if (manufacturer != null)
            sb.append(String.format("manufacturer:%s, ", manufacturer));
        if (alias != null)
            sb.append(String.format("alias:%s, ", alias));
        if (description != null)
            sb.append(String.format("description:%s, ", description));
        if (mark != null)
            sb.append(String.format("mark:%s.", mark));

        log.info(sb.toString());


        return bearingRepository.findAll(bearingSpecs.search(search, bearingId, name, fullname,
                        returnDoubleOrNull(priceMin),
                        returnDoubleOrNull(priceMax),
                        returnDoubleOrNull(din),
                        returnDoubleOrNull(dinMin),
                        returnDoubleOrNull(dinMax),
                        returnDoubleOrNull(dout),
                        returnDoubleOrNull(doutMin),
                        returnDoubleOrNull(doutMax),
                        returnDoubleOrNull(a),
                        returnDoubleOrNull(aMin),
                        returnDoubleOrNull(aMax),
                        manufacturer, description, alias, mark))
                .stream().map(bearingExchanger::exchangeToDTO).collect(Collectors.toUnmodifiableList());
    }

    private @Nullable Double returnDoubleOrNull(@Nullable String doubleValue){
        return doubleValue == null ? null : Double.parseDouble(doubleValue);
    }

}
