package org.bitbucket.semantor.services;

import org.bitbucket.semantor.repository.ImageRepository;

public interface ImageService {
    /**
     * This method trying to get the most suitable images from {@link ImageRepository}
     * and return it.
     * if there are no images at all, return empty array.
     *
     * @param match target matching
     * @return array of bytes
     */
    byte[] getImage(String match);
}
