package org.bitbucket.semantor.services;

import org.bitbucket.semantor.dto.MailDTO;

import java.util.List;

public interface MailService {
    List<MailDTO> getAllMailByUser(String user);

    MailDTO getMailByID(int mailId);
}
