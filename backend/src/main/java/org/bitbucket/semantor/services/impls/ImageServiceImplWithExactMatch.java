package org.bitbucket.semantor.services.impls;

import lombok.AllArgsConstructor;
import org.bitbucket.semantor.repository.ImageRepository;
import org.bitbucket.semantor.services.ImageService;

@AllArgsConstructor
public class ImageServiceImplWithExactMatch implements ImageService {
    private final ImageRepository imageRepository;

    @Override
    public byte[] getImage(String match) {
        return imageRepository.getImages(match);
    }
}
