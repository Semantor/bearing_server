package org.bitbucket.semantor.services;

import org.bitbucket.semantor.dto.BearingDTO;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface BearingSupplyAndRecordUserActivityService {
    List<BearingDTO> getAllBearings(@NotNull String userID);
    BearingDTO getBearingById(@NotNull String id,@NotNull String userID);
}
