package org.bitbucket.semantor.services;

import org.bitbucket.semantor.dto.BearingDTO;

import java.util.List;

public interface SearchBearingService {
    List<BearingDTO> search(String keyword);
    List<BearingDTO> searchByNameAndAlias(String s);
}
