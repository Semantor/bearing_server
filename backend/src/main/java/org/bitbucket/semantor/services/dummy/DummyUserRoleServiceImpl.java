package org.bitbucket.semantor.services.dummy;

import org.bitbucket.semantor.dto.UserDTO;
import org.bitbucket.semantor.services.UserServiceWithRolePermission;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class DummyUserRoleServiceImpl implements UserServiceWithRolePermission {

    @Override
    public List<UserDTO> getAllUser(@NotNull String userID) {
        throw new IllegalStateException("Not Implemented Yet");
    }

    @Override
    public UserDTO getUserByID(@NotNull String userId, @NotNull String requestingUser) {
        throw new IllegalStateException("Not Implemented Yet");
    }

    @Override
    public UserDTO saveUser(@NotNull UserDTO userDTO, String targetUserID, String userID) {
        throw new IllegalStateException("Not Implemented Yet");
    }
}
