package org.bitbucket.semantor.services.impls;

import lombok.AllArgsConstructor;
import org.bitbucket.semantor.data.object.UserSearch;
import org.bitbucket.semantor.repository.UserRepository;
import org.bitbucket.semantor.repository.UserSearchRepository;
import org.bitbucket.semantor.services.UserIdChecker;
import org.bitbucket.semantor.services.UserSearchRecorder;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@AllArgsConstructor
public class UserSearchRecorderImpl implements UserSearchRecorder {
    private final UserSearchRepository userSearchRepository;
    private final UserIdChecker userIdChecker;
    private final UserRepository userRepository;

    @Override
    public void addUserSearch(@NotNull String userId, @NotNull String search) {
        if (userIdChecker.isExistsUser(userId))
            userSearchRepository.save(
                    UserSearch.builder()
                            .search(search)
                            .user(userRepository.getById(UUID.fromString(userId)))
                            .build());

    }
}
