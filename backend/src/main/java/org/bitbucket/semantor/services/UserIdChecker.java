package org.bitbucket.semantor.services;

import org.springframework.lang.Nullable;

public interface UserIdChecker {

    /**
     * Check for {@link IllegalArgumentException} during {@link java.util.UUID#fromString(String)}
     * @param userId string to check
     * @return false if catch Exception
     */
    boolean isValidUuid(@Nullable String userId);

    /**
     * check for {@link IllegalArgumentException} during {@link java.util.UUID#fromString(String)}
     * and checks if such a user exists in the database with target {@link java.util.UUID}
     * @param userId string to check
     * @return true if exists
     */
    boolean isExistsUser(@Nullable String userId);
}
