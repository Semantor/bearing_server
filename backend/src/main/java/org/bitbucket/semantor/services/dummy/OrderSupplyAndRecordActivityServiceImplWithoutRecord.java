package org.bitbucket.semantor.services.dummy;

import lombok.AllArgsConstructor;
import org.bitbucket.semantor.dto.OrderDTO;
import org.bitbucket.semantor.services.OrderSupplierService;
import org.bitbucket.semantor.services.OrderSupplyAndRecordActivityService;

import java.util.List;

@AllArgsConstructor
public class OrderSupplyAndRecordActivityServiceImplWithoutRecord implements OrderSupplyAndRecordActivityService {
    private final OrderSupplierService orderSupplierService;

    @Override
    public OrderDTO getOrderById(long orderID, String userID) {
        return orderSupplierService.getOrderById(orderID);
    }

    @Override
    public List<OrderDTO> getUserOrder(String targetId, String requestingUserID) {
        return orderSupplierService.getUserOrder(targetId);
    }
}
