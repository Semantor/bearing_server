package org.bitbucket.semantor.services.impls;

import lombok.AllArgsConstructor;
import org.bitbucket.semantor.data.object.Order;
import org.bitbucket.semantor.data.object.Role;
import org.bitbucket.semantor.data.object.User;
import org.bitbucket.semantor.dto.OrderDTO;
import org.bitbucket.semantor.exchanger.OrderExchanger;
import org.bitbucket.semantor.repository.OrderRepository;
import org.bitbucket.semantor.repository.UserRepository;
import org.bitbucket.semantor.services.OrderSupplyAndRecordActivityService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
public class OrderSupplyAndRecordActivityServiceImpl implements OrderSupplyAndRecordActivityService {

    private final OrderRepository orderRepository;
    private final OrderExchanger orderExchanger;
    private final UserRepository userRepository;

    @Override
    public OrderDTO getOrderById(long orderID, String userID) {
        Optional<Order> byId = orderRepository.findById(orderID);
        if (byId.isPresent() && byId.get().getUser().getUuid().toString().equals(userID))
            return orderExchanger.exchangetoDTO(byId.get());
        return orderExchanger.exchangetoDTO(Order.DEFAULT_ORDER);
    }

    @Override
    public List<OrderDTO> getUserOrder(String targetId, String requestingUserID) {
        Optional<User> requester;
        Optional<User> target;
        if ((requester = userRepository.findById(UUID.fromString(requestingUserID))).isPresent()
                && (target = userRepository.findById(UUID.fromString(targetId))).isPresent()
                && requester.get().getRole().canReadElseOrderList)
            return orderRepository.findByUser(target.get())
                    .stream().map(orderExchanger::exchangetoDTO).collect(Collectors.toUnmodifiableList());
        return Collections.emptyList();
    }
}
