package org.bitbucket.semantor.services.impls;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.semantor.data.object.catalog.Bearing;
import org.bitbucket.semantor.dto.BearingDTO;
import org.bitbucket.semantor.exchanger.BearingExchanger;
import org.bitbucket.semantor.repository.BearingRepository;
import org.bitbucket.semantor.services.BearingSupplyingService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Slf4j
public class ManagerBearingSupplyingImpl implements BearingSupplyingService {

    private final BearingRepository bearingRepository;
    private final BearingExchanger bearingExchanger;

    @Override
    public BearingDTO getBearingById(int id) {
        Optional<Bearing> bearing = bearingRepository.findById(id);
        if (bearing.isEmpty())
            return bearingExchanger.exchangeToDTO(Bearing.emptyBearing);
        return bearingExchanger.exchangeToDTO(bearing.get());
    }

    public List<BearingDTO> searchByNameAndAlias(String s) {
        List<BearingDTO> bearingDTOS = new ArrayList<>();
        log.info("Searching by name");
        List<Bearing> bearings = bearingRepository.findByName(s);
        log.info("Searching by alias");
        bearings.addAll(bearingRepository.findByAlias(s));
        for (Bearing b :
                bearings) {
            bearingDTOS.add(bearingExchanger.exchangeToDTO(b));
        }
        return bearingDTOS;
    }

    @Override
    public List<BearingDTO> getAllBearings() {
        log.info("call method getAllBearing");
        List<BearingDTO> bearingDTOS = new ArrayList<>();
        for (Bearing b :
                bearingRepository.findAll()) {
            bearingDTOS.add(bearingExchanger.exchangeToDTO(b));
        }
        log.info("return result from method getAllBearing");
        return bearingDTOS;
    }

}
