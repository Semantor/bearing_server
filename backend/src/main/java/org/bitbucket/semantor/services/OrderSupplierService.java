package org.bitbucket.semantor.services;

import org.bitbucket.semantor.dto.OrderDTO;

import java.util.List;

public interface OrderSupplierService {

    /**
     * @param orderID to search
     * @return order, or empty order with id -1 and status {@link org.bitbucket.semantor.data.object.OrderStatus#ERROR}
     */
    OrderDTO getOrderById(long orderID);

    List<OrderDTO> getUserOrder(String userId);
}
