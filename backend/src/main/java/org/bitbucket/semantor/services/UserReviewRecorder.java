package org.bitbucket.semantor.services;

public interface UserReviewRecorder {
    void addUserReview(String userID, String bearingId);
}
