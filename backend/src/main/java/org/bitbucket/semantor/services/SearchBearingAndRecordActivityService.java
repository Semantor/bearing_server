package org.bitbucket.semantor.services;

import org.bitbucket.semantor.dto.BearingDTO;

import java.util.List;
import java.util.Properties;

@Deprecated
public interface SearchBearingAndRecordActivityService {
    List<BearingDTO> search(String key, String userID);

    List<BearingDTO> searchByNameAndAlias(String key, String userID);

    /**
     * STRING key for name bearing
     * STRING din in mm
     * STRING dout in mm
     * STRING a in mm
     * String manufacturer
     * STRING priceMin
     * STRING priceMax
     *
     * @param searchProperties to search
     * @param userID who are searching bearings
     * @return List of suitable bearing
     */
    List<BearingDTO> search(Properties searchProperties, String userID);
}
