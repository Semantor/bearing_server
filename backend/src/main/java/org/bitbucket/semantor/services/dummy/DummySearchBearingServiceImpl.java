package org.bitbucket.semantor.services.dummy;

import org.bitbucket.semantor.dto.BearingDTO;
import org.bitbucket.semantor.services.SearchBearingService;

import java.util.List;

public class DummySearchBearingServiceImpl implements SearchBearingService {
    @Override
    public List<BearingDTO> search(String keyword) {
        throw new IllegalStateException("not Implemented Yet");
    }

    @Override
    public List<BearingDTO> searchByNameAndAlias(String s) {
        throw new IllegalStateException("not Implemented Yet");
    }
}
