package org.bitbucket.semantor.services.dummy;

import org.bitbucket.semantor.data.object.UserReview;
import org.bitbucket.semantor.services.UserReviewSupplier;
import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.List;

public class DummyUserReviewSupplier implements UserReviewSupplier {
    @Override
    public List<UserReview> getUserReviews(@NotNull String userID, @NotNull String requestingUserID) {
        throw new IllegalStateException("not Implemented Yet");
    }

    @Override
    public List<UserReview> getReviewByDate(@NotNull Date from, @NotNull Date to, @NotNull String requestingUserID) {
        throw new IllegalStateException("not Implemented Yet");
    }

    @Override
    public List<UserReview> getReviewByBearing(int bearingId, @NotNull String requestingUserID) {
        throw new IllegalStateException("not Implemented Yet");
    }

    @Override
    public List<UserReview> getByAllArguments(String userID, Date from, Date to, int bearingID, @NotNull String requestingUserID) {
        throw new IllegalStateException("not Implemented Yet");
    }
}
