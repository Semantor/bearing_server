package org.bitbucket.semantor.services;

import org.bitbucket.semantor.dto.OrderDTO;

import java.util.List;

public interface OrderSupplyAndRecordActivityService {
    OrderDTO getOrderById(long orderID, String userID);
    List<OrderDTO> getUserOrder(String targetId, String requestingUserID);
}
