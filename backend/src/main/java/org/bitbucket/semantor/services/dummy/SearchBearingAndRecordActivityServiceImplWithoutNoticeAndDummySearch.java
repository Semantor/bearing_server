package org.bitbucket.semantor.services.dummy;

import lombok.AllArgsConstructor;
import org.bitbucket.semantor.dto.BearingDTO;
import org.bitbucket.semantor.services.SearchBearingAndRecordActivityService;
import org.bitbucket.semantor.services.SearchBearingService;

import java.util.List;
import java.util.Properties;

@AllArgsConstructor
public class SearchBearingAndRecordActivityServiceImplWithoutNoticeAndDummySearch implements SearchBearingAndRecordActivityService {
    private final SearchBearingService searchBearingService;
    @Override
    public List<BearingDTO> search(String key, String userID) {
        return searchBearingService.search(key);
    }

    @Override
    public List<BearingDTO> searchByNameAndAlias(String key, String userID) {
        return searchBearingService.searchByNameAndAlias(key);
    }

    @Override
    public List<BearingDTO> search(Properties searchProperties, String userID) {
        String name = searchProperties.getProperty("name", "");
        return searchBearingService.search(name);
    }
}
