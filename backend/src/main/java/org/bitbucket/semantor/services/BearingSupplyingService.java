package org.bitbucket.semantor.services;

import org.bitbucket.semantor.dto.BearingDTO;

import java.util.List;

public interface BearingSupplyingService {
    List<BearingDTO> getAllBearings();
    BearingDTO getBearingById(int id);
}
