package org.bitbucket.semantor.services.dummy;

import lombok.AllArgsConstructor;
import org.bitbucket.semantor.dto.BearingDTO;
import org.bitbucket.semantor.services.BearingSupplyAndRecordUserActivityService;
import org.bitbucket.semantor.services.BearingSupplyingService;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@AllArgsConstructor
public class BearingSupplyAndRecordUserActivity implements BearingSupplyAndRecordUserActivityService {
    private final BearingSupplyingService bearingSupplyingService;

    @Override
    public List<BearingDTO> getAllBearings(@NotNull String userID) {
        return bearingSupplyingService.getAllBearings();
    }

    @Override
    public BearingDTO getBearingById(@NotNull String id, @NotNull String userID) {
        return bearingSupplyingService.getBearingById(Integer.parseInt(id));
    }
}
