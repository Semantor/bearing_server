package org.bitbucket.semantor.services.impls;

import lombok.AllArgsConstructor;
import org.bitbucket.semantor.data.object.UserSearch;
import org.bitbucket.semantor.repository.UserRepository;
import org.bitbucket.semantor.repository.UserSearchRepository;
import org.bitbucket.semantor.repository.specs.UserSearchSpecs;
import org.bitbucket.semantor.repository.specs.UserSpecs;
import org.bitbucket.semantor.services.UserIdChecker;
import org.bitbucket.semantor.services.UserSearchSupplier;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

@AllArgsConstructor
public class UserSearchSupplierImpl implements UserSearchSupplier {
    private final UserIdChecker userIdChecker;
    private final UserRepository userRepository;
    private final UserSearchRepository userSearchRepository;
    private final UserSearchSpecs userSearchSpecs;

    @Override
    public List<UserSearch> getUserSearch(@Nullable String userID, @Nullable Date from, @Nullable Date to, @NotNull String requestingID) {
        if (!userIdChecker.isExistsUser(requestingID) ||
                !userRepository.getById(UUID.fromString(requestingID)).getRole().canReadUserSearch)
            return Collections.emptyList();
        return userSearchRepository.findAll(userSearchSpecs.search(
                userIdChecker.isValidUuid(userID) ? UUID.fromString(userID) : null,
                from,to,null));
    }
}
