package org.bitbucket.semantor.services;

import org.bitbucket.semantor.dto.UserDTO;

import java.util.List;

@Deprecated
public interface UserService {
    @Deprecated
    List<UserDTO> getAllUser();

    UserDTO getUserByID(String userId);

    boolean createNewUser(UserDTO userDTO);
}
