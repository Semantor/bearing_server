package org.bitbucket.semantor.services.impls;

import lombok.AllArgsConstructor;
import org.bitbucket.semantor.data.object.Order;
import org.bitbucket.semantor.data.object.OrderStatus;
import org.bitbucket.semantor.data.object.User;
import org.bitbucket.semantor.dto.OrderDTO;
import org.bitbucket.semantor.exchanger.OrderExchanger;
import org.bitbucket.semantor.repository.OrderRepository;
import org.bitbucket.semantor.repository.UserRepository;
import org.bitbucket.semantor.services.OrderRecorderWithActivityService;
import org.bitbucket.semantor.services.UserIdChecker;
import org.jetbrains.annotations.NotNull;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
public class OrderRecorderWithActivityServiceImpl implements OrderRecorderWithActivityService {
    private final OrderRepository orderRepository;
    private final OrderExchanger orderExchanger;
    private final UserRepository userRepository;
    private final UserIdChecker userIdChecker;

    @Override
    public @NotNull OrderDTO reserveOrder(@NotNull OrderDTO order, @NotNull String userID) {
        if (!userIdChecker.isExistsUser(userID)) return orderExchanger.exchangetoDTO(Order.DEFAULT_ORDER);
        Order newOrder = Order.builder()
                .user(userRepository.findById(UUID.fromString(userID)).get())
                .comments(order.getComments() == null ? new ArrayList<>() : order.getComments())
                .positions(order.getBuyingPositions() == null ? new ArrayList<>() : order.getBuyingPositions())
                .orderStatus(OrderStatus.CHECKING)
                .build();
        orderRepository.save(newOrder);
        return orderExchanger.exchangetoDTO(newOrder);
    }
}
