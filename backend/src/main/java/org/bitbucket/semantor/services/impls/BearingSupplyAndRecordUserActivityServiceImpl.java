package org.bitbucket.semantor.services.impls;

import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.bitbucket.semantor.ResponseTemplates;
import org.bitbucket.semantor.data.object.catalog.Bearing;
import org.bitbucket.semantor.dto.BearingDTO;
import org.bitbucket.semantor.exchanger.BearingExchanger;
import org.bitbucket.semantor.repository.BearingRepository;
import org.bitbucket.semantor.services.BearingSupplyAndRecordUserActivityService;
import org.bitbucket.semantor.services.UserIdChecker;
import org.bitbucket.semantor.services.UserReviewRecorder;
import org.jetbrains.annotations.NotNull;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Log
public class BearingSupplyAndRecordUserActivityServiceImpl implements BearingSupplyAndRecordUserActivityService {
    private final BearingRepository bearingRepository;
    private final UserReviewRecorder userReviewRecorder;
    private final BearingExchanger bearingExchanger;
    private final UserIdChecker userIdChecker;

    @Override
    public List<BearingDTO> getAllBearings(@NotNull String userID) {
        if (!userIdChecker.isExistsUser(userID)) {
            log.warning(String.format("%s in %s at %s", ResponseTemplates.WRONG_UUID,
                    this.getClass().getName(), new Timestamp(System.currentTimeMillis())));
            return Collections.emptyList();
        }
        return bearingRepository.findAll().stream().map(bearingExchanger::exchangeToDTO).collect(Collectors.toUnmodifiableList());
    }

    @Override
    public BearingDTO getBearingById(@NotNull String id, @NotNull String userID) {
        if (!userIdChecker.isExistsUser(userID)) {
            log.warning(String.format("%s in %s at %s", ResponseTemplates.WRONG_UUID,
                    this.getClass().getName(), new Timestamp(System.currentTimeMillis())));
            return bearingExchanger.exchangeToDTO(Bearing.emptyBearing);
        }

        Optional<Bearing> byId = bearingRepository.findById(Integer.valueOf(id));
        if (byId.isPresent()) {
            Bearing bearing = byId.get();
            userReviewRecorder.addUserReview(userID, id);
            return bearingExchanger.exchangeToDTO(bearing);
        }
        return bearingExchanger.exchangeToDTO(Bearing.emptyBearing);
    }
}
