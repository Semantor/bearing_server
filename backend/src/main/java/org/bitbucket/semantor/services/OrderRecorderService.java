package org.bitbucket.semantor.services;

import org.bitbucket.semantor.dto.OrderDTO;

public interface OrderRecorderService {
    boolean reserveOrder(OrderDTO order);
}
