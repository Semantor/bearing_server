package org.bitbucket.semantor.services;

import org.bitbucket.semantor.dto.BearingDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface BearingSearchService {
    List<BearingDTO> search(@NotNull String userid,
                            @Nullable String search,
                            @Nullable Integer bearingId,
                            @Nullable String name,
                            @Nullable String fullname,
                            @Nullable String priceMin,
                            @Nullable String priceMax,
                            @Nullable String din,
                            @Nullable String dinMin,
                            @Nullable String dinMax,
                            @Nullable String dout,
                            @Nullable String doutMin,
                            @Nullable String doutMax,
                            @Nullable String a,
                            @Nullable String aMin,
                            @Nullable String aMax,
                            @Nullable String manufacturer,
                            @Nullable String alias,
                            @Nullable String description,
                            @Nullable String mark
    );
}
