package org.bitbucket.semantor.services.impls;


import lombok.AllArgsConstructor;
import org.bitbucket.semantor.exchanger.UserExchanger;
import org.bitbucket.semantor.repository.UserRepository;
import org.bitbucket.semantor.data.object.User;
import org.bitbucket.semantor.dto.UserDTO;
import org.bitbucket.semantor.services.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
public class ManagerUserImpl implements UserService {
    private final UserRepository userRepository;
    private final UserExchanger userExchanger;

    @Override
    public List<UserDTO> getAllUser() {
        List<UserDTO> userDTOS = new ArrayList<>();
        for (User u :
                userRepository.findAll()) {
            userDTOS.add(userExchanger.exchangetoDTO(u));
        }
        return userDTOS;
    }

    @Override
    public UserDTO getUserByID(String id) {
        Optional<User> user;
        if ((user=userRepository.findById(UUID.fromString(id))).isPresent())
            return userExchanger.exchangetoDTO(user.get());
        return UserDTO.DEFAULT_USER;
    }

    @Override
    public boolean createNewUser(UserDTO userDTO) {
        User user = userExchanger.exchangetoDO(userDTO);
        userRepository.save(user);
        return true;
    }
}
