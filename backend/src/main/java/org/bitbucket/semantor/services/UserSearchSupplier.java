package org.bitbucket.semantor.services;

import org.bitbucket.semantor.data.object.UserSearch;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.List;

public interface UserSearchSupplier {
    List<UserSearch> getUserSearch(@Nullable String userID, @Nullable Date from, @Nullable Date to, @NotNull String requestingID);
}