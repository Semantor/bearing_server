package org.bitbucket.semantor.services;

import org.bitbucket.semantor.data.object.UserReview;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.List;

public interface UserReviewSupplier {
    List<UserReview> getUserReviews(@NotNull String userID, @NotNull String requestingUserID);
    List<UserReview> getReviewByDate(@NotNull Date from,@NotNull Date to,@NotNull String requestingUserID);
    List<UserReview> getReviewByBearing(int bearingId,@NotNull String requestingUserID);

    /**
     * some of the field can be nullable
     * @return list of suitable {@link UserReview}
     */
    List<UserReview> getByAllArguments(@Nullable String userID, @Nullable Date from, @Nullable Date to, int bearingID, @NotNull String requestingUserID);
}
