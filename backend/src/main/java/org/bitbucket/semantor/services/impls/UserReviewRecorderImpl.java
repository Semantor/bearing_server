package org.bitbucket.semantor.services.impls;

import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.bitbucket.semantor.data.object.User;
import org.bitbucket.semantor.data.object.UserReview;
import org.bitbucket.semantor.data.object.catalog.Bearing;
import org.bitbucket.semantor.repository.BearingRepository;
import org.bitbucket.semantor.repository.UserRepository;
import org.bitbucket.semantor.repository.UserReviewRepository;
import org.bitbucket.semantor.services.UserReviewRecorder;

import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
@Log
public class UserReviewRecorderImpl implements UserReviewRecorder {

    private final UserReviewRepository userReviewRepository;
    private final UserRepository userRepository;
    private final BearingRepository bearingRepository;
    @Override
    public void addUserReview(String userID, String bearingId) {
        Optional<User> user = userRepository.findById(UUID.fromString(userID));
        Optional<Bearing> bearing = bearingRepository.findById(Integer.parseInt(bearingId));
        if (user.isPresent()&&bearing.isPresent())
        userReviewRepository.save(UserReview.builder().user(user.get()).bearing(bearing.get()).build());
    }
}
