package org.bitbucket.semantor.services;

import org.bitbucket.semantor.data.object.Order;
import org.bitbucket.semantor.dto.OrderDTO;
import org.jetbrains.annotations.NotNull;

public interface OrderRecorderWithActivityService {
    @NotNull OrderDTO reserveOrder(@NotNull OrderDTO order,@NotNull  String userID );
}
