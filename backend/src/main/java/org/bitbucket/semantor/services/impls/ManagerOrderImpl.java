package org.bitbucket.semantor.services.impls;


import lombok.AllArgsConstructor;
import org.bitbucket.semantor.data.object.Order;
import org.bitbucket.semantor.data.object.User;
import org.bitbucket.semantor.dto.OrderDTO;
import org.bitbucket.semantor.exchanger.OrderExchanger;
import org.bitbucket.semantor.repository.OrderRepository;
import org.bitbucket.semantor.repository.UserRepository;
import org.bitbucket.semantor.services.OrderSupplierService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
public class ManagerOrderImpl implements OrderSupplierService {
    private final OrderRepository orderRepository;
    private final OrderExchanger orderExchanger;
    private final UserRepository userRepository;


    @Override
    public OrderDTO getOrderById(long id) {
        return orderExchanger.exchangetoDTO(orderRepository.findById(id).orElse(Order.builder().build()));
    }


    @Override
    public List<OrderDTO> getUserOrder(String userId) {
        Optional<User> findUser = userRepository.findById(UUID.fromString(userId));
        List<OrderDTO> orderDTOS = new ArrayList<>();
        if (findUser.isPresent()) {
            List<Order> orders = orderRepository.findByUser(findUser.get());
            for (Order o :
                    orders) {
                orderDTOS.add(orderExchanger.exchangetoDTO(o));
            }
        }
        return orderDTOS;
    }
}
