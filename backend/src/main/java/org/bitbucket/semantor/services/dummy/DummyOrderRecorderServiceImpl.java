package org.bitbucket.semantor.services.dummy;

import org.bitbucket.semantor.dto.OrderDTO;
import org.bitbucket.semantor.services.OrderRecorderService;

public class DummyOrderRecorderServiceImpl implements OrderRecorderService {
    @Override
    public boolean reserveOrder(OrderDTO order) {
        throw new IllegalStateException("not Implemented Yet");
    }
}
