package org.bitbucket.semantor.services.impls;

import lombok.AllArgsConstructor;
import org.bitbucket.semantor.data.object.User;
import org.bitbucket.semantor.data.object.UserReview;
import org.bitbucket.semantor.repository.UserRepository;
import org.bitbucket.semantor.repository.UserReviewRepository;
import org.bitbucket.semantor.repository.specs.UserReviewSpecs;
import org.bitbucket.semantor.services.UserIdChecker;
import org.bitbucket.semantor.services.UserReviewSupplier;
import org.jetbrains.annotations.NotNull;

import java.util.*;

@AllArgsConstructor
public class UserReviewSupplierImpl implements UserReviewSupplier {
    private final UserReviewRepository userReviewRepository;
    private final UserRepository userRepository;
    private final UserIdChecker userIdChecker;
    private final UserReviewSpecs userReviewSpecs;


    @Override
    public List<UserReview> getUserReviews(@NotNull String userID, @NotNull String requestingUserID) {
        if (canRequest(requestingUserID)&&userIdChecker.isExistsUser(userID))
           return userReviewRepository.findAll(userReviewSpecs.getByUserID(UUID.fromString(userID)));
        return Collections.emptyList();
    }

    @Override
    public List<UserReview> getReviewByDate(@NotNull Date from, @NotNull Date to, @NotNull String requestingUserID) {
        if (canRequest(requestingUserID))
            return userReviewRepository.findAll(userReviewSpecs.getByDateFrom(from).and(userReviewSpecs.getByDateTo(to)));
        return Collections.emptyList();
    }

    @Override
    public List<UserReview> getReviewByBearing(int bearingId, @NotNull String requestingUserID) {
        if (canRequest(requestingUserID))
            return userReviewRepository.findAll(userReviewSpecs.getByBearingId(bearingId));
        return Collections.emptyList();
    }

    @Override
    public List<UserReview> getByAllArguments(String userID, Date from, Date to, int bearingID, @NotNull String requestingUserID) {
        if (canRequest(requestingUserID)&&userIdChecker.isExistsUser(userID))
            return userReviewRepository.findAll(userReviewSpecs.search(UUID.fromString(userID),bearingID,from,to));
        return Collections.emptyList();
    }

    private boolean canRequest(String requestingID) {
        return userIdChecker.isExistsUser(requestingID)&&userRepository.getById(UUID.fromString(requestingID)).getRole().canReadUserReview;
    }
}