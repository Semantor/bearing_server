package org.bitbucket.semantor.services.impls;

import lombok.AllArgsConstructor;
import org.bitbucket.semantor.data.object.User;
import org.bitbucket.semantor.dto.UserDTO;
import org.bitbucket.semantor.exchanger.UserExchanger;
import org.bitbucket.semantor.data.object.UserHandler;
import org.bitbucket.semantor.repository.UserRepository;
import org.bitbucket.semantor.services.UserIdChecker;
import org.bitbucket.semantor.services.UserServiceWithRolePermission;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
public class UserServiceWithRolePermissionImpl implements UserServiceWithRolePermission {
    private final UserRepository userRepository;
    private final UserIdChecker userIdChecker;
    private final UserExchanger userExchanger;
    private final UserHandler userHandler;

    @Override
    public List<UserDTO> getAllUser(@NotNull String userID) {
        if (!canRead(userID))
            return Collections.emptyList();
        return userRepository.findAll().stream().map(userExchanger::exchangetoDTO).collect(Collectors.toUnmodifiableList());
    }

    @Override
    public UserDTO getUserByID(@NotNull String userId, @NotNull String requestingUser) {
        if (!canRead(requestingUser) || !userIdChecker.isExistsUser(userId))
            return UserDTO.DEFAULT_USER;
        return userExchanger.exchangetoDTO(userRepository.getById(UUID.fromString(userId)));
    }

    @Override
    public UserDTO saveUser(@NotNull UserDTO userDTO, String targetUserID, String requestingUserID) {
        if (userIdChecker.isExistsUser(targetUserID) && (
                userIdChecker.isExistsUser(requestingUserID) && targetUserID.equals(requestingUserID) ||
                        canRead(requestingUserID))) {
            User targetUser = userRepository.getById(UUID.fromString(targetUserID));
            userHandler.updateUser(targetUser, userDTO);
            return userExchanger.exchangetoDTO(targetUser);
        }
        return UserDTO.DEFAULT_USER;
    }


    private boolean canRead(@NotNull String userID) {
        return userIdChecker.isExistsUser(userID) ||
                userRepository.getById(UUID.fromString(userID)).getRole().canChangeElseProfile;
    }
}
