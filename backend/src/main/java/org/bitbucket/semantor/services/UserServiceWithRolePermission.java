package org.bitbucket.semantor.services;

import org.bitbucket.semantor.dto.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface UserServiceWithRolePermission {
    /**
     * checking role and return users.
     * can be use only for admins
     *
     * @param userID of requesting user
     * @return all users
     */
    List<UserDTO> getAllUser(@NotNull String userID);

    /**
     * can return to user themself or admin role
     *
     * @param userId  who is looking for
     * @param requestingUser who is looking
     * @return user info
     */
    UserDTO getUserByID(@NotNull String userId, @NotNull String requestingUser);

    /**
     * can be create(update due to logic) by user themself or by any other with appropriate privileges
     * @param userDTO information to update. Some fields mb @Nullabe or empty
     * @param targetUserID id which profile would be updated
     * @param requestingUserID id who requesting
     * @return {@link UserDTO#DEFAULT_USER} due to fail or update result
     */
    UserDTO saveUser(@NotNull UserDTO userDTO, @Nullable String targetUserID, @Nullable String requestingUserID);
}
