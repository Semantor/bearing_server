package org.bitbucket.semantor.data.object;

import org.jetbrains.annotations.NotNull;

public interface OrderHandler {

    void addPosition(@NotNull Order order,@NotNull BuyingPosition buyingPosition);

    void deletePosition(@NotNull Order order,@NotNull BuyingPosition buyingPosition);

    void changeCount(@NotNull BuyingPosition buyingPosition, int newCount);

    void addComment(@NotNull Order order,@NotNull OrderComment orderComment);
}
