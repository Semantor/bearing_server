package org.bitbucket.semantor.data.object;

import org.bitbucket.semantor.dto.UserDTO;

public interface UserHandler {
    void updateUser(User targetUser, UserDTO userDTO);

    boolean isValidPhone(String phone);

    boolean isValidSurname(String surname);

    boolean isValidEmail(String email);

    boolean isValidName(String name);
}
