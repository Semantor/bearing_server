package org.bitbucket.semantor.exchanger;


import org.bitbucket.semantor.data.object.catalog.Bearing;
import org.bitbucket.semantor.dto.BearingDTO;


public class BearingExchanger {
    public BearingDTO exchangeToDTO(Bearing bearing) {
        return BearingDTO.builder()
                .id(bearing.getId())
                .name(bearing.getName())
                .fullname(bearing.getFullname())
                .price(bearing.getPrice())
                .amount(bearing.getAmount())
                .din(bearing.getDin())
                .dout(bearing.getDout())
                .a(bearing.getA())
                .manufacturer(bearing.getManufacturer())
                .description(bearing.getDescription())
                .alias(bearing.getAlias())
                .mark(bearing.getMark())
                .build();
    }

    public Bearing exchangeToDO(BearingDTO bearingDTO) {
        return Bearing.builder()
                .name(bearingDTO.getName())
                .fullname(bearingDTO.getFullname())
                .price(bearingDTO.getPrice())
                .amount(bearingDTO.getAmount())
                .din(bearingDTO.getDin())
                .dout(bearingDTO.getDout())
                .a(bearingDTO.getA())
                .manufacturer(bearingDTO.getManufacturer())
                .description(bearingDTO.getDescription())
                .alias(bearingDTO.getAlias())
                .mark(bearingDTO.getMark())
                .build();
    }
}
