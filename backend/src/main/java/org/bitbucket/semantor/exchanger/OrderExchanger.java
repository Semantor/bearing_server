package org.bitbucket.semantor.exchanger;


import lombok.AllArgsConstructor;
import org.bitbucket.semantor.data.object.Order;
import org.bitbucket.semantor.dto.OrderDTO;


@AllArgsConstructor
public class OrderExchanger {
    private final UserExchanger userExchanger;

    public OrderDTO exchangetoDTO(Order order) {
        return OrderDTO.builder()
                .user(userExchanger.exchangetoDTO(order.getUser()))
                .buyingPositions(order.getPositions())
                .status(order.getOrderStatus())
                .comments(order.getComments())
                .build();
    }

    public Order exchangetoDO(OrderDTO orderDTO) {
        return Order.builder()
                .user(userExchanger.exchangetoDO(orderDTO.getUser()))
                .positions(orderDTO.getBuyingPositions())
                .orderStatus(orderDTO.getStatus())
                .comments(orderDTO.getComments())
                .build();
    }

}
