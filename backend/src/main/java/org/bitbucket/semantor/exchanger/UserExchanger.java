package org.bitbucket.semantor.exchanger;


import org.bitbucket.semantor.data.object.User;
import org.bitbucket.semantor.dto.UserDTO;


public class UserExchanger {
    public UserDTO exchangetoDTO(User user) {
        return UserDTO.builder()
                .email(user.getEmail())
                .name(user.getName())
                .surname(user.getSurname())
                .phone(user.getPhone())
                .city(user.getCity())
                .build();
    }

    public User exchangetoDO(UserDTO user) {
        return User.builder()
                .email(user.getEmail())
                .name(user.getName())
                .surname(user.getSurname())
                .phone(user.getPhone())
                .city(user.getCity())
                .build();
    }
}
