package org.bitbucket.semantor.exchanger;


import lombok.AllArgsConstructor;
import org.bitbucket.semantor.dto.MailDTO;
import org.bitbucket.semantor.data.object.Mail;

@AllArgsConstructor
public class MailExchanger {

    private final OrderExchanger orderExchanger;

    public MailDTO exchangetoDTO(Mail mail) {
        return MailDTO.builder()
                .orderDTO(orderExchanger.exchangetoDTO(mail.getOrder()))
                .theme(mail.getTheme())
                .text(mail.getText())
                .status(mail.getStatus())
                .build();
    }

    public Mail exchangetoDO(MailDTO mail) {
        return Mail.builder()
                .order(orderExchanger.exchangetoDO(mail.getOrderDTO()))
                .theme(mail.getTheme())
                .text(mail.getText())
                .status(mail.getStatus())
                .build();
    }
}
